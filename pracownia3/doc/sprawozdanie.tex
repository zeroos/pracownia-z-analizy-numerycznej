\documentclass[11pt,wide]{mwart}
\usepackage[utf8]{inputenc}
\usepackage[OT4,plmath]{polski}
\usepackage{hyperref}
\usepackage{bm}
\usepackage{array}
\usepackage{floatrow}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{csquotes}

\theoremstyle{plain}


\title{\textbf{Pracownia z analizy numerycznej}\\[1ex]
	   \large{Sprawozdanie do zadania \textbf{P3.20}}\\[0.5ex]
	   \large{Prowadzący: dr hab. Paweł Woźny}
}

\author{Barciś Michał}
\date{Wrocław, \today}


\newcolumntype{X}{>{\centering\arraybackslash}m{5cm}}
\newcolumntype{L}{>{\centering\arraybackslash}m{2.4cm}}

\begin{document}

\maketitle
\definecolor{green}{rgb}{0,0.5,0}

\section{Wstęp}

Metoda eliminacji Gaussa jest powszechnie uznawana jako jedna z najlepszych metod rozwiązywania układów równań liniowych\cite{historia-eliminacji}, jednak ma również wiele innych zastosowań, np. obliczanie rzędu macierzy, czy wyznaczanie wartości wyznacznika macierzy. Jej nazwa pochodzi od niemieckiego matematyka Carla Friedricha Gaussa, jednak wcale nie została przez niego wynaleziona. Pierwszy znany opis tej metody pojawił się już w 200 roku p.n.e. w chińskiej książce Chiu-chang Suan-shu, jednak do dzisiaj sprawia ona pewne problemy podczas obliczeń numerycznych.

Celem tego sprawozdania będzie przedstawienie tych problemów, oraz porównanie poprawności numerycznej oraz złożoności obliczeniowej czterech wersji metody eliminacji Gaussa -- bez wyboru elementu głównego, z wyborem elementu głównego w wierszach, kolumnach i z pełnym wyborem elementu głównego. 

\section{Opis metody eliminacji Gaussa}

\subsection{Definicje}
Przed opisem samej metody musimy wcześniej zdefiniować pojęcia operacji elementarnych oraz macierzy schodkowej.

\theoremstyle{definition}
\newtheorem*{operacje_elementarne}{Operacje elementarne}
\begin{operacje_elementarne}To takie przekształcenia macierzy, które nie zmieniają jej jądra. Oznacza to, że zbiór rozwiązań układu równań przez nią opisywanego nie zmienia się. Do takich operacji należą:

\begin{itemize}
	\item pomnożenie dowolnego wiersza przez liczbę różną od zera,
	\item do dowolnego wiersza dodanie innego pomnożonego przez liczbę,
	\item zamiana miejscami dowolnych dwóch wierszy.
\end{itemize}

\end{operacje_elementarne}


\newtheorem*{macierz_schodkowa}{Macierz schodkowa}
\begin{macierz_schodkowa}To taka macierz, której pierwszy element niezerowy każdego wiersza znajduje się w dalszej kolumnie niż pierwszy element niezerowy poprzedniego wiersza, czyli:

$$
\forall_{i,j} (a_{i,j} = 0 \wedge a_{i,j+1} \neq 0 \Rightarrow  \forall_{k > i, l \leq j+1} (a_{k,l} = 0))
$$

\end{macierz_schodkowa}

\subsection{Podstawowa metoda eliminacji}

Celem metody eliminacji Gaussa jest przekształcenie danej macierzy za pomocą operacji elementarnych do równoważnej macierzy schodkowej. Gdy mamy już macierz schodkową możemy np. bardzo łatwo obliczyć zbiór rozwiązań równania reprezentowanego przez tę macierz.

Sama metoda jest bardzo prosta. Na początku wybieramy pierwszy wiersz i odejmujemy od wszystkich pozostałych ten pierwszy pomnożony przez taką liczbę, aby w pierwszej kolumnie na wszystkich miejscach poza pierwszym było $0$. Analogicznie postępujemy dla kolejnych wierszy.

\subsection{Wybór elementów głównych}

Przedstawiona metoda ma jednak kilka wad. Po pierwsze, co zrobić, jeśli wartość w pierwszym wierszu (lub analogicznie kolejnych) to $0$? W takim wypadku algorytm zwróci błąd dzielenia przez 0.

W celu uniknięcia takiej sytuacji możemy przed przejściem do kolejnego wiersza zamienić go z jakimś innym tak, aby $a_{i,i} \neq 0$. Jeśli nie da się tego dokonać poprzez samą zamianę wierszy, można również zamienić kolumny. Taka metoda pozwala nam już na podanie rozwiązań każdego układu równań\cite{nicholas_higham}.

Nie rozwiązuje to jednak innego, znacznie poważniejszego problemu. Jeśli macierz jest źle uwarunkowana, a obliczenia wykonujemy w arytmetyce zmiennoprzecinkowej, to wynik obliczony tą metodą może okazać się bardzo niedokładny. W związku z tym zaproponowano modyfikację tej metody, która za każdym razem wybiera wiersz w taki sposób, żeby na początku zawsze pojawiała się największa liczba. Dzięki temu po wykonaniu dzielenia zawsze otrzymamy liczbę mniejszą niż jeden, co pozwoli zredukować występowanie zjawiska utraty cyfr znaczących. Największą liczbę na odpowiednim miejscu można uzyskać również poprzez wybór kolumn lub zarówno wierszy jak i kolumn. Ten ostatni wariant nazywany jest pełnym wyborem elementu głównego.

\section{Złożoność obliczeniowa}

Testy wydajnościowe procedur zostały przedstawione w tabeli \ref{tbl:zlozonosc}. Były one wykonane na losowych macierzach o różnych wielkościach i gęstościach. Jak widać, zgodnie z intuicją, wersja bez wyboru elementu głównego jest najszybsza. Pozostałe algorytmy wykonują te same operacje co ona, oraz dodatkowo zamieniają wiersze lub kolumny miejscami, więc muszą wykonywać się dłużej. Algorytm z pełnym wyborem elementu głównego jest znacznie wolniejszy niż metody z jego wyborem w kolumnach lub wierszach, co również zgodne jest z intuicją -- aby wybrać największy element w całym wycinku macierzy trzeba przejrzeć jego wszystkie elementy, podczas gdy do znalezienia największego elementu w kolumnie wystarczy przejrzeć tylko elementy z tej kolumny.

Niestety powyższe operacje nie sprawdzają się dla macierzy o małej gęstości ($\le 50\%$). Najprawdopodobniej spowodowane jest to optymalizacjami wykonywanymi przez Maple dla macierzy rzadkich i dzielenia 0.

\begin{table}[h]
\begin{tabular}{|X|L|L|L|L|L|}
\hline
	& \textbf{Z pełnym wyborem elementu głównego} & \textbf{Z wyborem elementu głównego w kolumnach} & \textbf{Z wyborem elementu głównego w wierszach} & \textbf{Bez wyboru elementu głównego} \\
\hline
	50x50, wartość max $10^{10}$ & 2.594s & 1.984s & 2.032s & 1.750s\\
\hline
	25x25, wartość max $10^{10}$ & 0.063s & 0.062s & 0.063s & 0.062s\\
\hline
	25x25, wartość max 100 & 0.641s & 0.531s & 0.657s & 0.562s\\
\hline
	50x50, gęstość 25\% & 2.141s & 1.734s & 1.750s & 2.187s\\
\hline
	50x50, gęstość 50\% & 2.266s & 1.812s & 1.735s & 2.093s\\
\hline
	50x50, gęstość 75\% & 2.281s & 2.563s & 2.390s & 2.094s\\
\hline
	złożoność obliczeniowa & $\Theta((m \cdot n)^2)$ & $\Theta(m \cdot n^2)$ & $\Theta(m^2 \cdot n)$ & $\Theta(m \cdot n)$\\
\hline
\end{tabular}
\caption{Porównanie złożoności obliczeniowej różnych wersji metody eliminacji Gaussa dla testowych danych}
\label{tbl:zlozonosc}
\end{table}


\section{Poprawność numeryczna}

W tym akapicie przedstawione zostaną testy poprawności numerycznej opisywanych wcześniej metod. Niektóre z wykorzystanych macierzy inspirowane były przykładami z książki \enquote{Accuracy and stability of numerical algorithms}\cite{nicholas_higham}.

Wszystkie testy numeryczne przeprowadzono przy użyciu systemu algebry komputerowej \textsf{Maple 17} symulując tryb pojedynczej precyzji (tj. przyjmując \texttt{Digits := 8}).


Kolorem \textcolor{green}{zielonym} oznaczone są wyniki oczekiwane, wyliczone z większą dokładnością, a kolorem \textcolor{blue}{niebieskim} wyniki poprawne, wyliczone przez program w trybie pojedynczej precyzji.

Na początek przeanalizowany zostanie przykład następującej macierzy $2x2$:
\begin{equation}
\begin{bmatrix}
\epsilon & 1 \\
1 & 1
\end{bmatrix}
\label{eqn:przyklad1}
\end{equation}
gdzie $\epsilon$ jest bardzo małą liczbą, tutaj konkretnie $10^{-34}$. Wyniki przeprowadzenia różnych wariantów eliminacji Gaussa dla tej macierzy zostały przedstawione na rysunku \ref{fig:przyklad1}. 

W tym przypadku zastosowanie metody z wyborem kolumny wystarczył, aby uzyskać poprawny wynik. Zarówno metoda bez wyboru elementu głównego jak i z wyborem wiersza dały wyniki obarczone bardzo dużym błędem ($1 \over \epsilon$).

\begin{figure}[h]
        \centering
        \begin{subfigure}[b]{0.19\textwidth}
                \centering
            	$$\begin{bmatrix}
				10^{-34} & 1 \\
				0 & -10^{34}
				\end{bmatrix}
				$$
                \caption{bez wyboru}
        \end{subfigure}
        \begin{subfigure}[b]{0.19\textwidth}
                \centering
            	$$\begin{bmatrix}
				1 & 1 \\
				0 & 1
				\end{bmatrix}
				$$
                \caption{wybór wiersza}
        \end{subfigure}
        \begin{subfigure}[b]{0.19\textwidth}
                \centering
            	$$\textcolor{blue}{\begin{bmatrix}
				1 & 10^{-34} \\
				0 & 1
				\end{bmatrix}}
				$$
                \caption{wybór kolumny}
        \end{subfigure}
        \begin{subfigure}[b]{0.19\textwidth}
                \centering
            	$$\textcolor{blue}{\begin{bmatrix}
				1 & 10^{-34} \\
				0 & 1
				\end{bmatrix}}
				$$
                \caption{pełny wybór}
        \end{subfigure}
        \begin{subfigure}[b]{0.19\textwidth}
                \centering
            	$$\textcolor{green}{\begin{bmatrix}
				1 & 10^{-34} \\
				0 & 1
				\end{bmatrix}}
				$$
                \caption{poprawny wynik}
        \end{subfigure}
        \label{fig:przyklad1}
        \caption{Wyniki różnych wariantów metody eliminacji Gaussa dla macierzy \ref{eqn:przyklad1}.}
\end{figure}

Wyniki przeprowadzonej metody eliminacji Gaussa dla kolejnej macierzy przedstawione są na rysunku \ref{fig:przyklad2}. Tym razem aby uzyskać poprawny wynik należy wykorzystać metodę eliminacji Gaussa z pełnym wyborem elementu głównego. Ciekawą obserwacją jest również fakt, że dla tych metod uzyskana macierz nie jest nawet macierzą schodkową -- poniżej przekątnej znajdują się niezerowe wartości.

\begin{equation}
\begin{bmatrix}
1 & -1 & 2 & 0 \\
2 & 5 & 1 & 0 \\
-1 & 2 & 1 & -2 \\
0 & 0 & 4 & 0 \\
\end{bmatrix}
\label{eqn:przyklad2}
\end{equation}

\begin{figure}[h]
        \centering
        \begin{subfigure}[b]{0.49\textwidth}
                \centering
            	$$\begin{bmatrix}
				1 & -1 & 2 & 0 \\
				0 & 7 & -3 & 0 \\
				0 & 2 \cdot 10^{-8} & 3.4285714 & -2 \\
				0 & -2.3333334 \cdot 10^{-8} & -1 \cdot 10^{-7} & 2.3333334
				\end{bmatrix}
				$$
                \caption{bez wyboru}
        \end{subfigure}
        \begin{subfigure}[b]{0.3\textwidth}
                \centering
            	$$\begin{bmatrix}
				2 & 5 & 1 & 0 \\
				0 & 4.5 & 1.5 & -2 \\
				0 & 0 & 4 & 0 \\
				0 & 0 & 0 & -1.5555556
				\end{bmatrix}
				$$
                \caption{wybór wiersza}
        \end{subfigure}
        \begin{subfigure}[b]{0.33\textwidth}
                \centering
            	$$\begin{bmatrix}
				2 & -1 & 1 & 0 \\
				0 & 5.5 & 1.5 & 0 \\
				0 & 0 & -2.1818182 & -2 \\
				0 & 0 & -1 \cdot 10^{-7} & 2.3333332
				\end{bmatrix}
				$$
                \caption{wybór kolumny}
        \end{subfigure}
        \begin{subfigure}[b]{0.3\textwidth}
                \centering
            	$$\textcolor{blue}{\begin{bmatrix}
				5 & 1 & 0 & 2 \\
				0 & 4 & 0 & 0 \\
				0 & 0 & -2 & -1.8 \\
				0 & 0 & 0 & 1.4
				\end{bmatrix}}
				$$
                \caption{pełny wybór}
        \end{subfigure}
        \begin{subfigure}[b]{0.3\textwidth}
                \centering
            	$$\textcolor{green}{\begin{bmatrix}
				5 & 1 & 0 & 2 \\
				0 & 4 & 0 & 0 \\
				0 & 0 & -2 & -1.8 \\
				0 & 0 & 0 & 1.4
				\end{bmatrix}}
				$$
                \caption{poprawny wynik}
        \end{subfigure}
        \label{fig:przyklad2}
        \caption{Wyniki różnych wariantów metody eliminacji Gaussa dla macierzy \ref{eqn:przyklad2}.}
\end{figure}

\section{Wnioski}

Metoda eliminacji Gaussa jest bardzo użytecznym, skutecznym i wszechstronnym narzędziem. Niestety, jak to się często zdarza, i w tym wypadku musimy wybierać wersję metody szukając kompromisu pomiędzy jakością numeryczną rozwiązania, a jego szybkością. Dodatkowo algorytm może nie działać dobrze dla źle uwarunkowanych macierzy, jednak problem ten występuje zawsze podczas wykonywania wielu operacji na dużych macierzach.

\section{Dodatek: Uruchamianie programu}

W pliku \texttt{program.mw} zostały zdefiniowane następujące procedury wykonujące metodę eliminacji Gaussa na podanej w argumencie macierzy:

\begin{description}
	\item[basicElimination] metoda eliminacji Gaussa bez wyboru elementu głównego,
	\item[eliminationWithRowSelection] z wyborem elementu głównego w wierszach,
	\item[eliminationWithColumnSelection] z wyborem elementu głównego w kolumnach,
	\item[eliminationWithFullSelection] z pełnym wyborem elementu głównego.
\end{description}

Pod deklaracją procedur zamieszczony jest przykład użycia programu.

\begin{thebibliography}{9}

\bibitem{historia-eliminacji}
  \url{http://math.berkeley.edu/~mgu/Seminar/Fall2010/Sept08_2010.pdf}
  
\bibitem{historia-eliminacji-meyer}
  \url{http://meyer.math.ncsu.edu/Meyer/PS_Files/GaussianEliminationHistory.pdf}
  
\bibitem{nicholas_higham}
Higham, Nicholas J.. "9. LU Factorization and Linear Equations" Accuracy and stability of numerical algorithms. 2nd ed. Philadelphia: Society for Industrial and Applied Mathematics, 2002. 157-192. Print.

\end{thebibliography}


\end{document}