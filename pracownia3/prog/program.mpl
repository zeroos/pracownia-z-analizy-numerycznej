Digits := 8;

with(LinearAlgebra);
with(MTM);
with(ListTools);
with(CodeTools);

Debug := false;
%;
Numerical := false;

maxIndex := proc (L) local i, maxValue, result; maxValue := max(abs(L)); result := None; for i to Dimension(L) do if abs(L[i]) = maxValue then result := i; break end if end do; result end proc;
%;
# NULL;
gaussianElimination := proc (M, mode) local i, j, jRow, iRow, multipliedRow; for i to RowDimension(M) do if Debug then print("---", i, "---") end if; if mode = rowSelection then rowSwapping(M, i) elif mode = columnSelection then columnSwapping(M, i) elif mode = fullSelection then fullSwapping(M, i) end if; iRow := M[i, 1 .. ColumnDimension(M)]; for j from i+1 to RowDimension(M) do jRow := M[j, 1 .. ColumnDimension(M)]; if Debug then print(iRow, jRow) end if; multipliedRow := ScalarMultiply(iRow, jRow[i]/iRow[i]); if Numerical then multipliedRow := evalf(multipliedRow) end if; M[j, 1 .. ColumnDimension(M)] := Minus(jRow, multipliedRow); if Numerical then M[j, 1 .. ColumnDimension(M)] := evalf(M[j, 1 .. ColumnDimension(M)]) end if end do; if Debug then print(M) end if end do; M end proc;
# 
rowSwapping := proc (M, col) local i, maxRow, tempRow; maxRow := maxIndex(M[col .. (), col])+col-1; tempRow := M[col, () .. ()]; M[col, () .. ()] := M[maxRow, () .. ()]; M[maxRow, () .. ()] := tempRow; maxRow end proc;
%;
# 
columnSwapping := proc (M, row) local i, maxColumn, tempColumn; maxColumn := maxIndex(M[row, row .. ()])+row-1; tempColumn := M[() .. (), row]; M[() .. (), row] := M[() .. (), maxColumn]; M[() .. (), maxColumn] := tempColumn; maxColumn end proc;
%;
# 
fullSwapping := proc (M, i) local row, column, maxValue, maxRow, maxColumn, temp; maxValue := max(abs(M[i .. (), i .. ()])); maxRow := None; maxColumn := None; for row from i to RowDimension(M) do if maxRow <> None then break end if; for column from i to ColumnDimension(M) do if abs(M[row, column]) = maxValue then maxRow := row; maxColumn := column end if end do end do; if Debug then print(maxColumn, maxRow) end if; temp := M[() .. (), i]; M[() .. (), i] := M[() .. (), maxColumn]; M[() .. (), maxColumn] := temp; temp := M[i, () .. ()]; M[i, () .. ()] := M[maxRow, () .. ()]; M[maxRow, () .. ()] := temp end proc;


basicElimination := proc (M) options operator, arrow; gaussianElimination(M, basic) end proc;

eliminationWithRowSelection := proc (M) options operator, arrow; gaussianElimination(M, rowSelection) end proc;
eliminationWithColumnSelection := proc (M) options operator, arrow; gaussianElimination(M, columnSelection) end proc;
eliminationWithFullSelection := proc (M) options operator, arrow; gaussianElimination(M, fullSelection) end proc;

# 
Org := RandomMatrix(3, 3, generator = rand(10^40)*(1/rand(10^40)));

A := evalf(Org); A1 := copy(A); A2 := copy(A); A3 := copy(A); A4 := copy(A);



linalg[cond](A);
# 
# 


evalf(eliminationWithFullSelection(Org));
eliminationWithFullSelection(A1);

eliminationWithColumnSelection(A2);

eliminationWithRowSelection(A3);
basicElimination(A4);

# 
