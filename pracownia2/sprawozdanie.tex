\documentclass[11pt,wide]{mwart}
\usepackage[utf8]{inputenc}
\usepackage[OT4,plmath]{polski}
\usepackage{hyperref}
\usepackage{bm}
\usepackage{floatrow}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{subcaption}
\usepackage{graphicx}

\theoremstyle{plain}


\title{\textbf{Pracownia z analizy numerycznej}\\[1ex]
	   \large{Sprawozdanie do zadania \textbf{P2.20}}\\[0.5ex]
	   \large{Prowadzący: dr hab. Paweł Woźny}
}

\author{Barciś Michał\\
		Chabinka Tomasz}
\date{Wrocław, \today}

\begin{document}

\maketitle

\section{Wstęp}

Skalowanie jest operacją polegającą na zmianie obrazu zapisanego w formie cyfrowej. Problem poprawnego skalowania zyskiwał na znaczeniu w ostatnich latach -- wraz z rozwojem Internetu oraz cyfryzacją niemal każdej dziedziny życia. Skalowanie obrazu wykorzystywane jest między innymi do tworzenia miniaturek wykorzystywanych na stronach internetowych, dopasowywania plików graficznych do stale zwiększających się rozdzielczości monitorów czy w wykorzystujących cyfrową telewizję naziemną DVB-T odbiornikach telewizyjnych.

Skalowanie nie jest procesem łatwym. Wybranie najodpowiedniejszego dla danego zadania algorytmu to w praktyce wybór między wydajnością, gładkością i ostrością uzyskanego w wyniku skalowania obrazu.

Niniejsza praca omawia trzy najpopularniejsze algorytmy skalowania. Pod analizę wzięta została zarówno ich wydajność jak i jakość skalowania. Pokazano również artefakty, które mogą powstawać przy wykorzystaniu poszczególnych metod i podjęto próbę wytłumaczenia ich źródła. Ponadto uzyskane wyniki porównane zostały z wynikami uzyskiwanymi przez profesjonalny program do obróbki grafiki rastrowej.

\section{Algorytmy skalowania obrazu}

\subsection{Opis}

W ramach rozwiązywania zadania zaimplementowane zostały trzy najczęściej wykorzystywane algorytmy skalowania obrazu:
\begin{enumerate}
\item metoda najbliższego sąsiada (\texttt{nearest-neigbor interpolation})
\item z wykorzystaniem funkcji sklejanej pierwszego stopnia (\texttt{bilinear interpolation})
\item z wykorzystaniem funkcji sklejanej trzeciego stopnia (\texttt{cubic interpolation})
\end{enumerate}

Zadaniem każdego z algorytmów jest obliczenie wartości koloru $K(\cdot)$ w punktach $p_i = 1 + (i - 1)\frac{M-1}{N-1}$, gdzie M -- obecny rozmiar obrazu, N -- nowy rozmiar obrazu, $i = (1, 2,\dots, N)$. Do wstępnego ich porównania użyta została prosta grafika 10x3 px w każdym wierszu zawierająca jeden gradient. W pierwszym (czerwony) o liniowym przyroście koloru, w drugim (zielony) o kwadratowym przyroście koloru, a w trzecim (niebieski) o sześciennym przyroście koloru.

Metoda najbliższego sąsiada jest najprostsza. Wyznacza wartość $K(p_i)$ jako wartość koloru najbliższego piksela na nieprzeskalowanym obrazie: $K(p_i) := K(round(p_i))$, $i = (1, 2,\dots, N)$. Przeskalowany za jej pomocą gradient zaprezentowany jest na rysunku \ref{fig:gradienty-simple}. Uzyskany obraz nie jest już gradientem dla żadnego z wejściowych wierszy, a przejścia kolorów są krokowe i wyraźnie widoczne.

\begin{figure}[h]
    \centering
    \includegraphics[height=2cm,width=\textwidth]{testy-wyniki/gradienty-simple.png}
    \caption{Gradienty przeskalowane metodą najbliższego sąsiada, rozciągnięte w pionie.}
    \label{fig:gradienty-simple}
\end{figure}

Medody wykorzystujące funkcje sklejane są bardziej złożone. Wyznaczając wartość $K(p_i)$ biorą pod uwagę kolory dwóch najbliższych pikseli na nieprzeskalowanym obrazie w sposób odpowiedni dla danej funkcji sklejanej. Dla skonstruowanej na podstawie punktów $K(i)$, $i= (1, 2,\dots, M)$ funkcji sklejanej pierwszego stopnia S i funkcji sklejanej trzeciego stopnia Z mamy więc: $K(p_i) := S( p_i )$ oraz $K(p_i) := Z( p_i )$.

Przeskalowane za ich pomocą gradienty przedstawiają rysunki \ref{fig:gradienty-nfsi1} oraz \ref{fig:gradienty-nfsi3}. Przejścia między kolorami są już płynne -- niezauważalne. 

\begin{figure}[h]
    \centering
    \includegraphics[height=2cm,width=\textwidth]{testy-wyniki/gradienty-nfsi1.png}
    \caption{Gradienty przeskalowane za pomocą NFSI1, rozciągnięte w pionie.}
    \label{fig:gradienty-nfsi1}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[height=2cm,width=\textwidth]{testy-wyniki/gradienty-nfsi3.png}
    \caption{Gradienty przeskalowane za pomocą NFSI3, rozciągnięte w pionie.}
	\label{fig:gradienty-nfsi3}
\end{figure}


Wynik zastosowania poszczególnych algorytmów (dwukrotne zwiększenie wysokości i szerokości) na przykładowym obrazie przedstawiającym krajobraz przedstawia rysunek \ref{fig:algorithms_result}. Wyniki wszystkich skalowań są zbliżone do siebie, chociaż można zauważyć pewne różnice. Przykładem może być sposób skalowania przez poszczególne rozwiązania liści drzew widocznych. W przypadku algorytmów wykorzystujących funkcje sklejane drzewa są "gładsze", niż w przypadku algorytmu najbliższych sąsiadów.

\begin{figure}[h]
        \centering
        \begin{subfigure}[b]{0.49\textwidth}
                \centering
                \includegraphics[scale=0.115]{testy/background.jpg}
                \caption{Oryginał}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \centering
                \includegraphics[scale=0.0575]{testy-wyniki/background-simple.jpg}
                \caption{Prosty}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \centering
                \includegraphics[scale=0.0575]{testy-wyniki/background-nfsi1.jpg}
                \caption{NFSI1}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \centering
                \includegraphics[scale=0.0575]{testy-wyniki/background-nfsi3.jpg}
                \caption{NFSI3}
        \end{subfigure}
        \caption{Wynik działania poszczególnych algorytmów skalowania}\label{fig:algorithms_result}
\end{figure}

\subsection{Kolejność skalowania poszczególnych osi}

Należy zauważyć, że każdy z algorytmów skalowania działa w ten sposób, że najpierw skaluje obraz w jednej osi, a dopiero później -- na podstawie uzyskanego wyniku -- skaluje drugą oś. Taki sposób działania algorytmów może budzić wątpliwości, czy wybór kolejności skalowania osi ma wpływ na ostateczny wynik.

Z przeprowadzonych w programie testów wynika, że różnica rzeczywiście istnieje i obejmuje znaczną powierzchnię obrazka jednak jej wartość jest niewielka (co do modułu równa 1), przez co jest ona niezauważalna dla ludzkiego oka.

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.49\textwidth}
                \centering
                \includegraphics[scale=0.115]{testy/background.jpg}
                \caption{Oryginał}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \centering
                \includegraphics[scale=0.0575]{testy-wyniki/background-diff-simple.jpg}
                \caption{Prosty}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \centering
                \includegraphics[scale=0.0575]{testy-wyniki/background-diff-nfsi1.jpg}
                \caption{NFSI1}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \centering1
                \includegraphics[scale=0.0575]{testy-wyniki/background-diff-red-nfsi3.jpg}
                \caption{NFSI3}
        \end{subfigure}
        \caption{Obrazy różnicowe dla różnych metod skalowania}\label{fig:diff_all}
\end{figure}

Rysunek \ref{fig:diff_all}. pokazuje punkty (oznaczne kolorem czerwonym), które różniły się wartością w obrazie wynikowym skalowanym pion-poziom i w obrazie wynikowym skalowanym poziom-pion. Zauważyć można, że brak było takich punktów w obrazie skalowanym za pomocą metody najbliższych sąsiadów. Ponadto obrazy różnicowe wygenerowane przez metody wykorzystujące funkcje sklejane są takie same.

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.49\textwidth}
                \centering
                \includegraphics[scale=0.115]{testy/background.jpg}
                \caption{Oryginał}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \centering
                \includegraphics[scale=0.0575]{testy-wyniki/background-diff-red-nfsi3.jpg}
                \caption{Czerwona składowa}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \centering
                \includegraphics[scale=0.0575]{testy-wyniki/background-diff-green-nfsi3.jpg}
                \caption{Zielona składowa}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \centering
                \includegraphics[scale=0.0575]{testy-wyniki/background-diff-blue-nfsi3.jpg}
                \caption{Niebieska składowa}
        \end{subfigure}
        \caption{Obrazy różnicowe dla metody NFSI3 dla różnych kolorów składowych}\label{fig:diff_nfsi3}
\end{figure}

Rysunek \ref{fig:diff_nfsi3} pozwala porównać obrazy różnicowe wygenerowane dla różnych kolorów składowych (czerwonego, zielonego i niebieskiego). Zauwazyć można, że różnice kolorów nie powstają w obszarach, gdzie na dużej powierzchni kolor jest podobny. Ponadto wygląd obrazów różnicowych dla różnych kolorów składowych jest do siebie bardzo mocno zbliżony. 

\subsection{Złożoność i rzeczywisty czas działania}

Na podstawie analizy poszczególnych algorytmów ich teoretyczną złożoność można określić jako:

\begin{itemize}
\item $\Theta(M_y\cdot N_x+N_x\cdot N_y)$ dla algorytmu najbliższych sąsiadów,
\item $\Theta(M_y\cdot N_x+N_x\cdot N_y)$ dla algorytmu wykorzystującego naturalną funkcję sklejaną pierwszego stopnia,
\item $\Theta(M_y\cdot (M_x+N_x)+N_x\cdot (N_x + N_y))$ dla algorytmu wykorzystującego naturalną funkcję sklejaną trzeciego stopnia.
\end{itemize}

Eksperymentalna analiza czasu działania algorytmów (Tabela \ref{T:Tab1}.) wskazuje, że w rzeczywistości dominujące znaczenie ma czas operacji wejścia/wyjścia -- odczytu i zapisu skalowanego obrazu. Analizie poddano dwa sposoby wykonania operacji wejścia/wyjścia:
\begin{itemize}
\item wczytanie obrazka do lokalnej zmiennej typu \texttt{vector< vector<int> >} na początku działania programu, zapis obrazka z takiej samej struktury na końcu działania programu i operowanie na odwołaniach do lokalnej zmiennej typu \texttt{vector< vector<int> >} w trakcie działania programu (Sposób 1.),
\item posługiwanie się w trakcie działania programu bezpośrednimi odwołaniami (z wykorzystaniem funkcji bibliotecznych biblioteki ImageMagick) do plików z obrazkami (Sposób 2.).
\end{itemize}

\begin{table}[!h]
\renewcommand{\arraystretch}{1.1}
\begin{center}

\caption{Wyniki eksperymentalnego pomiaru czasu działania programu dla algorytmu NFSI1}
\label{T:Tab1}
\vspace{1ex}
\begin{center}
\begin{tabular}{r|l|l|l}
Sposób & Rozmiar obrazu wejściowego & Rozmiar obrazu wyjściowego & Czas działania \\ \hline
1 & 960x600 & 1000x1000 & 2.884s \\
1 & 960x600 & 10000x10000 & 3m36.636s \\
1 & 19200x12000 & 1000x1000 & 3m1.876s \\
2 & 960x600 & 1000x1000 & 4.933s \\  
2 & 960x600 & 10000x10000 & 7m37.894s \\
2 & 19200x12000 & 1000x1000 & 31.086s \\
\end{tabular}
\end{center}
\end{center}
\end{table}

W przypadku gdy mamy mało danych do odczytu i wielokrotnie odwołujemy się do każdej z odczytanych liczb znacznie lepiej sprawdza się strategia z wczytaniem całego obrazka do wewnętrznej pamięci programu. W przypadku gdy wejściowy obraz jest bardzo duży, a do utworzenia wyjściowego obrazka będziemy potrzebowali tylko niewielkej ilości odczytów lepiej sprawdzi się odczytywanie wartości wprost z pliku obrazu.

Analogiczne wnioski można wyciągnąć dla przypadku wykorzystania algorytmu najbliższych sąsiadów. W przypadku wykorzystania naturalnej funkcji sklejanej trzeciego stopnia zawsze lepiej sprawdzi się natomiast sposób wykorzystujący wczytanie pliku do pamięci wewnętrznej. Jest tak, ponieważ tworzenie funkcji sklejanych dla poszczególnych kolumn i wierszy wykorzystuje wielokrotne odwołanie się do każdej z wartości obrazka wejściowego.

\section{Artefakty}

Nierozłącznym elementem skalowania obrazów jest brak jakichś informacji. Podczas przekształcania większego obrazu do mniejszego w obrazie wyjściowym możemy przekazać mniej informacji, co wiąże się z ich utratą. W drugą stronę natomiast na wejściu mamy mniej danych niż oczekuje się na wyjściu programu, więc część z nich musi zostać wygenerowana.

Z oboma tymi problemami różne algorytmy radzą sobie z innymi efektami, jednak można wyróżnić kilka charakterystycznych artefaktów, czyli sztucznych zjawisk, które często tworzą się podczas skalowania grafik i obrazów.

Na początku tej sekcji zostaną opisane dwa z takich artefaktów (rozmycie obrazu oraz prążki Moiré). Podane również będzie intuicyjne wyjaśnienie ich występowania. Następnie zostanie podjęta próba bardziej naukowego podejścia do problemu przy pomocy teorii sygnałów i okaże się, że tak naprawdę geneza obu tych artefaktów jest taka sama.

\subsection{Rozmycie}

Efekt rozmycia najłatwiej zobrazować poprzez powiększanie bardzo małych obrazków, co prezentuje rysunek \ref{fig:rozmycie_up}. Wynika on z faktu, iż algorytm nie bierze pod uwagę krawędzi obiektów przedstawionych na obrazie i rozciąga je tak samo jak każdy inny punkt. Przez to stają się mniej ostre, rozmazane.

\begin{figure}[h]
        \centering
        \begin{subfigure}[b]{0.15\textwidth}
                \centering
                \includegraphics[scale=1]{testy/samochod.png}
                \caption{Oryginał}
        \end{subfigure}
        \begin{subfigure}[b]{0.23\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/rozmycie_up-simple.png}
                \caption{Prosty}
        \end{subfigure}
        \begin{subfigure}[b]{0.23\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/rozmycie_up-nfsi1.png}
                \caption{NFSI1}
        \end{subfigure}
        \begin{subfigure}[b]{0.23\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/rozmycie_up-nfsi3.png}
                \caption{NFSI3}
        \end{subfigure}
        \caption{Efekt rozmycia powstały na skutek powiększania obrazu }\label{fig:rozmycie_up}
\end{figure}

Czasami ten efekt można również zaobserwować podczas pomniejszania obrazka (rysunek \ref{fig:rozmycie_down}). W tym przypadku wynika on z tego, że kolor niektórych punktów nie może zostać jednoznacznie określony ponieważ składają się na niego informacje z kilku sąsiednich punktów. Wtedy część algorytmów uśrednia wartość koloru w danym punkcie tworząc tym samym obraz rozmyty. Warto zauważyć, że wystarczy minimalne skalowania grafiki -- te przedstawione na rysunku \ref{fig:rozmycie_down} zostały przeskalowane z 30x30 px do 29x29px. Przy algorytmach, które nie biorą pod uwagę uśrednionych wartości (np. metoda najbliższego sąsiedztwa) nie występuje ten efekt. Dlatego właśnie tego typu algorytmy używa się do skalowania bardzo małych ikonek.

\begin{figure}[h]
        \centering
        \begin{subfigure}[b]{0.15\textwidth}
                \centering
                \includegraphics[scale=1]{testy/kratka-duza.png}
                \caption{Oryginał}
        \end{subfigure}
        \begin{subfigure}[b]{0.23\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/rozmycie_down-simple.png}
                \caption{Prosty}
        \end{subfigure}
        \begin{subfigure}[b]{0.23\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/rozmycie_down-nfsi1.png}
                \caption{NFSI1}
        \end{subfigure}
        \begin{subfigure}[b]{0.23\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/rozmycie_down-nfsi3.png}
                \caption{NFSI3}
        \end{subfigure}
        \caption{Efekt rozmycia powstały na skutek pomniejszania obrazu. Oprogramowanie wyświetlające ten dokument może samo nieznacznie przeskalować obraz oryginalny, przez co również pojawi się na nim rozmycie -- w rzeczywistości jest on wyłącznie czarno biały. Zaleca się podgląd oryginalnego obrazka, który dostarczony jest wraz z kodem źródłowym tego dokumentu.}\label{fig:rozmycie_down}
\end{figure}

\subsection{Prążki Moiré}

Prążki Moiré to zjawisko powstałe na skutek nałożenia się na siebie dwóch (lub więcej) siatek linii obróconych o pewien kąt lub poddanych deformacji. Podczas skalowania wzorce, które wstępują okresowo, w specyficznych warunkach mogą ulec zniekształceniu (więcej o tym w sekcji \ref{sec:aliasing}). Gdy zniekształcenie to nałoży się na siebie w dwóch wymiarach powstaje specyficzny wzór (zależy od zniekształcenia) zwany Prążkami Moiré. 

Rysunek \ref{fig:moire} przedstawia różne wzory powstałe na skutek skalowania rysunku \ref{fig:flaga-wspolnoty-narodow}. Na pierwszych z nich widoczne są charakterystyczne prążki Moiré.

\begin{figure}[H]
        \centering
        \includegraphics[scale=0.5]{testy/flaga-wspolnoty-narodow.png}
        \caption{Flaga wspólnoty narodów - obrazek wejściowy do prezentacji prążków Moiré, przeskalowany o 50\% w celu ułatwienia prezentacji w tym dokumencie.}
		\label{fig:flaga-wspolnoty-narodow}
\end{figure}

\begin{figure}[h]
        \centering
        \begin{subfigure}[b]{0.23\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/moire1.png}
                \caption{prążki Moiré}
        \end{subfigure}
        \begin{subfigure}[b]{0.23\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/moire2.png}
                \caption{40x40 px}
        \end{subfigure}
        \begin{subfigure}[b]{0.23\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/moire3.png}
                \caption{30x30 px}
        \end{subfigure}
        \caption{Różne wzory powstałe na skutek skalowania rysunku \ref{fig:flaga-wspolnoty-narodow}.}
        \label{fig:moire}
\end{figure}




\subsection{Aliasing}
\label{sec:aliasing}

Kluczem do zrozumienia źródła występowania opisanych powyżej artefaktów jest teoria przetwarzania sygnałów, a konkretniej twierdzenie Whittakera-Nyquista-Kotielnikova-Shannona oraz zjawisko aliasingu.

Opis tej teorii znacznie wybiega poza zakres tego sprawozdania, jednak podstawowe pojęcia i fakty z nią związane są niezbędne do zrozumienia tego problemu i będą one używane w dalszej części tej sekcji.

Na początku zostanie przedstawione pewne zjawisko, które później spróbujemy wytłumaczyć. Na rysunku \ref{fig:freq} przedstawiony jest biało-czarny gradient, który staje się coraz gęstszy bliżej prawej strony. Rysunek \ref{fig:freq-alg} przedstawia wynik pomniejszania tego gradientu za pomocą różnych algorytmów, a rysunek \ref{fig:freq-sizes} wynik pomniejszania do różnych wielkości za pomocą algorytmu NFSI1.


\begin{figure}[h]
\centering
\includegraphics[scale=1]{testy/freq.png}
\caption{Nieprzeskalowany rysunek użyty do badania zjawiska aliasingu. Źródło: \url{http://www.esiee.fr/infoweb/projets/i4/lapierrg/rapport/exploitation.html}}
\label{fig:freq}
\end{figure}

\begin{figure}[h]
        \centering
        \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/freq-simple.png}
                \caption{Metoda najbliższego sąsiada}
        \end{subfigure}
        \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/freq-nfsi1.png}
                \caption{NFSI1}
        \end{subfigure}
        \begin{subfigure}[b]{0.45\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/freq-nfsi3.png}
                \caption{NFSI3}
        \end{subfigure}
        \caption{Porównanie skalowania grafiki z rysunku \ref{fig:freq} za pomocą różnych algorytmów.}
        \label{fig:freq-alg}
\end{figure}

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/freq-480.png}
                \caption{szerokość: 480px}
        \end{subfigure}
        \begin{subfigure}[b]{0.6\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/freq-300.png}
                \caption{szerokość: 300px}
        \end{subfigure}
        \begin{subfigure}[b]{0.39\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/freq-150.png}
                \caption{szerokość: 150px}
        \end{subfigure}
        \begin{subfigure}[b]{0.3\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/freq-100.png}
                \caption{szerokość: 100px}
        \end{subfigure}
        \begin{subfigure}[b]{0.33\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/freq-50.png}
                \caption{szerokość: 50px}
        \end{subfigure}
        \begin{subfigure}[b]{0.33\textwidth}
                \centering
                \includegraphics[scale=1]{testy-wyniki/freq-30.png}
                \caption{szerokość: 30px}
        \end{subfigure}
        \caption{Porównanie skalowania grafiki z rysunku \ref{fig:freq} do różnych rozdzielczości za pomocą metody najbliższego sąsiada.}
        \label{fig:freq-sizes}
\end{figure}

Zauważmy, że na rysunku \ref{fig:freq-alg} niezależnie od algorytmu występuje takie samo zjawisko -- niektóre białe paski łączą się w jeden grubszy pasek, a inne znikają. Zawsze są to te same paski. Z drugiej jednak strony na rysunku \ref{fig:freq-sizes} w zależności od rozmiaru inne paski znikają i inne łączą się.

\subsubsection{Obraz jako sygnał}

Zjawisko to łatwo wytłumaczyć korzystając z teorii przetwarzania sygnałów. Rozpatrzmy rysunek \ref{fig:freq}. Dane w kolumnach możemy pominąć -- służą one tylko prezentacji faktu, że zjawisko to ma miejsce niezależnie od natężenia koloru. W związku z tym weźmy ostatni z wierszy, ponieważ jego dynamika jest największa, i potraktujmy go jako jednowymiarowy sygnał. Poczynione obserwacje oczywiście będą prawdziwe również dla dwuwymiarowych obrazów, ponieważ algorytmy z których korzystamy i tak operują tylko na poszczególnych liniach.

W kolejnych akapitach będziemy również pisać o próbkowaniu sygnału. Podczas skalowania w dół zawsze w jakiś sposób wykonujemy próbkowanie sygnału. Metoda najbliższego sąsiedztwa wręcz opiera się wyłącznie na próbkowaniu obrazu wejściowego. Algorytmy oparte na interpolacji naturalną funkcją sklejaną ustalają wartość pomiaru za pomocą większej ilości niż jednej próbki. W ogólności można by było traktować zbiór punktów które wpływają na wynik pomiaru jako jedną próbkę lekko zaburzoną.

\subsubsection{Utrata danych podczas próbkowania sygnału}

Do zrozumienia czemu sygnał wejściowy (obrazek) został zaburzony podczas próbkowania (pomniejszania) potrzebne jest już tylko jedno narzędzie:

\newtheorem*{mydef}{Twierdzenie Whittakera-Nyquista-Kotielnikova-Shannona (o próbkowaniu)}

\begin{mydef}
Jeśli sygnał ciągły nie posiada składowych widma o częstotliwości równej lub większej niż B, to może on zostać wiernie odtworzony z ciągu jego próbek tworzących sygnał dyskretny, o ile próbki te zostały pobrane w odstępach czasowych nie większych niż 1/(2B).\cite{twierdzenie-kotielnikowa-shannona}
\end{mydef}

Niestety nie mówi nam ono jednoznacznie o tym kiedy i w jaki sposób sygnał zostanie zaburzony, ale daje nam informację o tym, które składowe mogą ulec deformacji. Jeśli taka deformacja nastąpi obraz zostanie nieodwracalnie zniekształcony (wystąpi zjawisko aliasingu), a w tym sygnale pojawią się składowe o błędnych częstotliwościach (aliasy).

Rysunek \ref{fig:widmo-wejsciowe} przedstawia widmo sygnału wejściowego uzyskane za pomocą transformacji Fouriera. Widzimy na nim dosyć płaski wykres ze szczytem w okolicach niskich częstotliwości. Jako, że sygnał wejściowy był sygnałem dyskretnym to obok widma oryginalnego sygnału (oznaczonego niebieskawym tłem) pojawiają się jego kopie przesunięte o wszystkie całkowite wielokrotności częstotliwości próbkowania. 

\begin{figure}[H]
    \centering
    \includegraphics[scale=1]{testy-wyniki/widmo-wejsciowe.png}
    \caption{Wykres transformaty Fouriera dla rysunku \ref{fig:freq}.}
    \label{fig:widmo-wejsciowe}
\end{figure}


Widzimy również, że niektóre częstotliwości w ogóle nie występują -- z twierdzenia o próbkowaniu wiemy, iż oznacza to, że można tak dobrać częstotliwość próbkowania aby nie utracić informacji o sygnale. Zobaczmy jednak jak wygląda widmo obrazka przeskalowanego, na którym wyraźnie widać utratę pewnych informacji. Rysunek \ref{fig:widmo-przeskalowane} przedstawia transformatę Fouriera dla obrazu wejściowego przeskalowanego do szerokości 100px. Wyraźnie widać na nim, że widmo oryginalny nałożyło się na swoje powtórzenie, w związku z czym wysokie częstotliwości zostały zdeformowane -- pojawił się aliasing.

\begin{figure}[H]
    \centering
    \includegraphics[scale=1]{testy-wyniki/widmo-przeskalowane.png}
    \caption{Wykres transformaty Fouriera dla rysunku \ref{fig:freq} przeskalowanego za pomocą metody najbliższego sąsiada do szerokości 100px.}
    \label{fig:widmo-przeskalowane}
\end{figure}

\subsubsection{Konsekwencje}

Czemu więc na przeskalowanym obrazie niektóre białe paski stały się grubsze, a inne w ogóle się nie pojawiły? Zaburzone zostały te częstotliwości sygnału, które w tych konkretnych miejscach obrazka miały znaczący wpływ na jego wartość.

Gdy dwie takie deformacje nałożą się na siebie w obrazie dwuwymiarowym możemy uzyskać różne wzorce, np. opisywane wcześniej prążki Moiré.

Zjawisko aliasingu jest również bezpośrednio związane z zaobserwowanym wcześniej rozmywaniem się obrazu -- ostre krawędzie reprezentowane są przez składowe sygnału o wysokiej częstotliwości, które w związku z tym zjawiskiem zostają zdeformowane.

\subsubsection{Rozwiązania problemu}

Istnieje jednak sposób radzenia sobie z tym problemem. Wystarczy przed skalowaniem obrazka nałożyć na niego filtr dolnoprzepustowy (np. rozmycie Gaussa) dzięki któremu pozbędziemy się wysokich częstotliwości występujących w obrazie. Taki obraz przeskalowany będzie wolny od opisanych wyżej defektów. Jednak oczywiście uzyskana w ten sposób grafika nie będzie pomniejszeniem grafiki początkowej, tylko rozmytej grafiki początkowej.

\section{Jakość skalowania}\label{sec:quality}

O tym, że w związku z akapitem `Wstęp` jakość trudno ocenić, ponieważ zawsze coś jest kosztem czegoś. Spróbujemy jednak przedstawić nasze pomysły na ocenę jakości, ew. porównanie różnych algorytmów.

\section{Wnioski}

Jak zostało pokazane w niniejszej pracy nie istnieje obiektywnie najlepsza metoda skalowania obrazów. Każda z nich ma swoje wady i swoje zalety. Czasami - jak w przypadku skalowania w dół biało-czarnych obrazów - najlepiej sprawdzi się metoda najbliższego sąsiedztwa, a w innym przypadku okaże się, że najlepsza będzie jedna z metod wykorzystujących naturalne funkcje sklejane. Za każdym razem projektant rozwiązania powinien pamiętać, że wybiera między efektywnością, gładkością i ostrością otrzymanego wyniku.

Zagadnienie skalowania najlepiej rozpatrywać w kontekście teorii sygnałów. Tłumaczy ona tak niedoskonałość skalowania "zwykłych obrazków" jak i powstawanie artefaktów, takich jak rozmycie czy prążki Moiré. Z dokonań teorii sygnałów skorzystali twórcy metody Lanczos. Jak pokazano w Sekcji \ref{sec:quality}. daje ona w przypadku skalowania "zwykłych" obrazów najlepsze wyniki.

Można spodziewać się, że wraz z dalszym rozwojem Internetu (w szczególności korzystającego z ograniczonego zakresu częstotliwości internetu bezprzewodowego) znaczenie problemu skalowania, oraz powiązanego z nim problemu interpolacji sygnału cyfrowego pomiędzy próbkami, będzie rosło.

\section{Dodatek: Kompilacja i uruchamianie programu}

\subsection{Kompilacja programu}

Aby skompilować program niezbędna jest biblioteka magick++ dostępna w pakiecie ImageMagick. Strona domowa projektu: \url{http://www.imagemagick.org/Magick++/}.

Do kodu dostarczony jest plik Makefile w związku z czym kompilacja powinna powieść się po uruchomieniu polecenia "make" w katalogu z kodem źródłowym.

\subsection{Uruchamianie programu}

W związku z zadaniem wykonaliśmy dwa programy: scale oraz difference. Ten pierwszy służy do skalowania obrazów, drugi do stworzenia obrazu różnicowego.

\subsubsection{scale}

Sposób użycia: \\
\indent\indent./scale plik\_wejściowy algorytm szerokość wysokość [plik\_wyjściowy]

Gdzie algorytm może być jednym z:\\
\begin{itemize}
\item simple,
\item NFSI1,
\item NFSI3.
\end{itemize}

Jeśli plik wyjściowy nie zostanie podany to wynik zostanie wyświetlony w nowym oknie.

\subsubsection{difference}

Sposób użycia: \\
\indent\indent./difference plik\_wejściowy kolor algorytm szerokość wysokość [plik\_wyjściowy]

Gdzie algorytm może być jednym z:\\
\begin{itemize}
\item simple,
\item NFSI1,
\item NFSI3.
\end{itemize}

A kolor może być jednym z:\\
\begin{itemize}
\item red,
\item green,
\item blue.
\end{itemize}

Jeśli plik wyjściowy nie zostanie podany to wynik zostanie wyświetlony w nowym oknie.

\begin{thebibliography}{9}

\bibitem{twierdzenie-kotielnikowa-shannona}
  \url{http://pl.wikipedia.org/wiki/Twierdzenie_Kotielnikowa-Shannona}
  
\bibitem{przetwarznie-sygnalu}
Lyons, Richard G., and Jan Zarzycki. "2.1. Aliasing: niejednoznaczność sygnału w dziedzinie częstotliwości." Wprowadzenie do cyfrowego przetwarzania sygnałów. Warszawa: Wydawnictwa Komunikacji i Łączności, 1999. 37-42.

\end{thebibliography}


\end{document}