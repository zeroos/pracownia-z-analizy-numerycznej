/*
 * simpletransformationbw.cpp
 *
 *  Created on: 8 gru 2013
 *      Author: tch
 */

#include "simpletransformationbw.hpp"
#include <vector>
#include <cmath>
#include <iostream>

using namespace std;

SimpleTransformationBW::SimpleTransformationBW( vector< vector<int> >* newImage ) {
	image = newImage;
};

void SimpleTransformationBW::transformRow( vector<int>& row, int newSize ) {
	vector<int> oldRow = row;
	int oldSize = oldRow.size();

	row.resize(newSize);

	for(int i = 0; i < newSize;i++) {
		row[i] = oldRow.at( roundpi(i, newSize, oldSize) );
	}
}

int SimpleTransformationBW::roundpi( int i, int newSize, int oldSize) {
	int divider = newSize - 1 != 0 ? newSize - 1 : 1;
	int result = (int)round( i*((oldSize-1.0)/divider) );
	return result > oldSize-1 ? oldSize-1 : result;
}

void SimpleTransformationBW::scaleX(int newSize) {
	for( unsigned int i = 0; i < image->size(); i++) {
		transformRow( image->at(i), newSize );
	}
}

void SimpleTransformationBW::scaleY(int newSize) {
	vector< vector<int> > oldImage = (*image);
	int oldSize = oldImage.size();
	int firstRowSize = oldImage.at(0).size();

	image->resize(newSize);

	for(int y = 0; y < newSize;y++) {
		(*image)[y].resize(firstRowSize);
		for(int x = 0; x < firstRowSize; x++) {
			(*image)[y][x] = oldImage.at( roundpi(y, newSize, oldSize) ).at(x);
		}
	}
}

void SimpleTransformationBW::scaleXY(int newX, int newY) {
	scaleX(newX);
	scaleY(newY);
}

void SimpleTransformationBW::scaleYX(int newX, int newY) {
	scaleY(newY);
	scaleX(newX);
}

SimpleTransformationBW::~SimpleTransformationBW() {

}
