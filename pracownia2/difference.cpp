#include <Magick++/Color.h>
#include <Magick++/Geometry.h>
#include <Magick++/Image.h>
//#include <Magick++.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

#include "nfsi1transformationbw.hpp"
#include "nfsi3transformationbw.hpp"
#include "simpletransformationbw.hpp"

using namespace std;
using namespace Magick;

void usage(){
    cout << "Sposob uzycia:\t./difference plik_wejsciowy kolor algorytm szerokosc wysokosc [plik_wyjsciowy]" << endl
         << endl
         << "Gdzie algorytm może być jednym z:" << endl
         << "\tsimple" << endl
         << "\tNFSI1" << endl
         << "\tNFSI3" << endl
         << endl
         << "A kolor może być jednym z :" << endl
         << "\tred" << endl
         << "\tgreen" << endl
         << "\tblue" << endl
         << endl
         << "Jesli plik wyjsciowy nie zostanie podany to wynik zostanie wyswietlony w nowym oknie" << endl;
}

void previewVectorImage(vector< vector<int> > vectorImage)
{
    cout << " --- preview - begining [" << vectorImage.size() << "x" << vectorImage.at(0).size() << "]--- " << endl;
    for(unsigned int y=0; y<vectorImage.size(); y++){
        for(unsigned int x=0; x<vectorImage.at(y).size(); x++){
            cout << vectorImage.at(y).at(x) << " ";
        }
        cout << endl;
    }
    cout << " --- preview - end --- " << endl;
}

int main( int argc, char ** argv)
{
    if(argc < 6){
        usage();
        return 1;
    }
    string input_file = argv[1];
    string color_s = argv[2];
    string algorithm = argv[3];
    int width = atoi(argv[4]);
    int height = atoi(argv[5]);
    string output_file = "";

    if(argc > 6){//will be replaced by getopts or sth
        output_file = argv[6];
    }
    InitializeMagick(*argv);
    Image image = Image(input_file);
    cout << "Zaladowano plik '" << input_file << "'." << endl;
    cout << "Zostanie on przeskalowany przy uzyciu algorytmu '" << algorithm
         << "' do rozdzielczosci " << width << "x" << height << "." << endl;

    //transform Magick++ image to vectorImage
    vector< vector<int> > vectorImageXY = vector< vector<int> >();
    vector< vector<int> > vectorImageYX = vector< vector<int> >();
    for(unsigned int y=0; y<image.rows(); y++){
        vector<int> rowXY;
        vector<int> rowYX;
        for(unsigned int x=0; x<image.columns(); x++){
            Color color = image.pixelColor(x,y);
            if( color_s.compare("red") == 0 )
            	rowXY.push_back((int)color.redQuantum());
            else if( color_s.compare("green") == 0 )
            	rowXY.push_back((int)color.greenQuantum());
            else if( color_s.compare("blue") == 0 )
            	rowXY.push_back((int)color.blueQuantum());
            else {
                cout << "Nieobslugiwany algorytm skalowania." << endl;
                usage();
                return 2;
            }
        }
        rowYX = rowXY;
        vectorImageXY.push_back(rowXY);
        vectorImageYX.push_back(rowYX);
    }

    //transform it
    if(algorithm.compare("simple") == 0){
    	SimpleTransformationBW transformationXY(&vectorImageXY);
    	SimpleTransformationBW transformationYX(&vectorImageYX);
    	transformationXY.scaleXY(width, height);
    	transformationYX.scaleYX(width, height);
    }else if(algorithm.compare("NFSI1") == 0){
    	NFSI1TransformationBW transformationXY(&vectorImageXY);
    	NFSI1TransformationBW transformationYX(&vectorImageYX);
    	transformationXY.scaleXY(width, height);
    	transformationYX.scaleYX(width, height);
    }else if(algorithm.compare("NFSI3") == 0){
    	NFSI3TransformationBW transformationXY(&vectorImageXY);
    	NFSI3TransformationBW transformationYX(&vectorImageYX);
    	transformationXY.scaleXY(width, height);
    	transformationYX.scaleYX(width, height);
    }else{
        cout << "Nieobslugiwany algorytm skalowania." << endl;
        usage();
        return 2;
    }

    int maxV = 0;
    //transform it back to Magick++
    Image outputImage = Image(Geometry(width, height), "pink");
    for(unsigned int y=0; y<vectorImageXY.size(); y++){
        for(unsigned int x=0; x<vectorImageXY.at(y).size(); x++){
        	maxV = max(maxV, abs(vectorImageXY.at(y).at(x) - vectorImageYX.at(y).at(x)) );

            outputImage.pixelColor(x, y, Color(
            		color_s.compare("red") == 0 ? QuantumRange*abs(vectorImageXY.at(y).at(x) - vectorImageYX.at(y).at(x)) : 0,
            	    color_s.compare("green") == 0 ? QuantumRange*abs(vectorImageXY.at(y).at(x) - vectorImageYX.at(y).at(x)) : 0,
            	    color_s.compare("blue") == 0 ? QuantumRange*abs(vectorImageXY.at(y).at(x) - vectorImageYX.at(y).at(x)) : 0
                                              )
            );
        }
    }

    cout << "Maksymalna różnica wyniosła: " << maxV << endl;

    if(output_file.compare("") == 0){
        cout << "Wyswietlam wynik w nowym oknie." << endl;
        outputImage.display();
    }else{
        outputImage.write(output_file);
        cout << "Plik zostal zapisany jako " << output_file << "." << endl;
    }
    return 0;
}
