/*
 * nfsi1transformation.cpp
 *
 *  Created on: 8 gru 2013
 *      Author: tch
 */

#include "nfsi1transformationbw.hpp"
#include <vector>
#include <cmath>
#include <iostream>

using namespace std;

NFSI1TransformationBW::NFSI1TransformationBW( vector< vector<int> >* newImage ) {
	image = newImage;
};

void NFSI1TransformationBW::transformRow( vector<int>& row, int newSize ) {
	vector<int> oldRow = row;
	int oldSize = oldRow.size();

	row.resize(newSize);

	for(int i = 0; i < newSize;i++) {
		double pi_r = pi( i, newSize, oldSize);
		int floor_r = floor(pi_r);
		double rest = pi_r - floor_r;
		if( floor_r == pi_r )
			row[i] = oldRow[floor_r];
		else
			row[i] = (oldRow[floor_r+1] - oldRow[floor_r])*rest + oldRow[floor_r] ;
	}

}

double NFSI1TransformationBW::pi( int i, int newSize, int oldSize) {
	int divider = newSize - 1 != 0 ? newSize - 1 : 1;
	double result = i*((oldSize-1.0)/(divider));
	return result > oldSize-1 ? oldSize-1 : result;
}

void NFSI1TransformationBW::scaleX(int newSize) {
	for( unsigned int i = 0; i < image->size(); i++) {
		transformRow( image->at(i), newSize );
	}
}

void NFSI1TransformationBW::scaleY(int newSize) {
	vector< vector<int> > oldImage = (*image);
	int oldSize = oldImage.size();
	int firstRowSize = oldImage.at(0).size();

	image->resize(newSize);

	for(int y = 0; y < newSize;y++) {
		(*image)[y].resize(firstRowSize);
		for(int x = 0; x < firstRowSize; x++) {
			double pi_r = pi( y, newSize, oldSize);
			int floor_r = floor(pi_r);
			double rest = pi_r - floor_r;
			if( rest == 0 )
				(*image)[y][x] = oldImage.at(pi_r).at(x);
			else
				(*image)[y][x] = (oldImage.at(floor_r+1).at(x) - oldImage.at(floor_r).at(x))*rest + oldImage.at(floor_r).at(x) ;
		}
	}
}

void NFSI1TransformationBW::scaleXY(int newX, int newY) {
	scaleX(newX);
	scaleY(newY);
}

void NFSI1TransformationBW::scaleYX(int newX, int newY) {
	scaleY(newY);
	scaleX(newX);
}

NFSI1TransformationBW::~NFSI1TransformationBW() {
}
