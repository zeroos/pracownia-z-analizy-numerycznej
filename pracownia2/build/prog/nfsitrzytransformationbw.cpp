#include "nfsitrzytransformationbw.hpp"
#include <vector>
#include <cmath>
#include <iostream>

using namespace std;

NFSI3TransformationBW::NFSI3TransformationBW( vector< vector<int> >* newImage ) {
	image = newImage;
};

void NFSI3TransformationBW::transformRow( vector<int>& row, int newSize ) {
	vector<int> oldRow = row;
	int oldSize = oldRow.size();
	prepareValuesForNFSI3(oldRow);

	row.resize(newSize);

	for(int i = 0; i < newSize;i++) {
		double pi_r = pi( i, newSize, oldSize);
		int floor_r = floor(pi_r);
		if( floor_r == pi_r )
			row[i] = oldRow[pi_r];
		else
			row[i] = s(pi_r, oldRow);
	}
}

double NFSI3TransformationBW::pi( int i, int newSize, int oldSize) {
	double divider = newSize - 1 != 0 ? newSize - 1 : 1;
	double result = i*((oldSize-1.0)/divider);
	return result > oldSize-1 ? oldSize-1 : result;
}

void NFSI3TransformationBW::prepareValuesForNFSI3(std::vector<int>& row) {
	int size = row.size();
	q = vector<double>();
	u = vector<double>();
	p = vector<double>();
	d = vector<double>();
	M = vector<double>();

	q.resize(size);
	u.resize(size);
	p.resize(size);
	d.resize(size);
	M.resize(size+1);

	q[0] = 0;
	u[0] = 0;

	for( int i = 0; i < size; i++) {
		double delta = 1/2;
		p[i] = delta * q[i-1] + 2;
		q[i] = (delta - 1)/p[i];

		d[i] = 3*(row[i+1] - 2*row[i] + row[i-1]);
		u[i] = (d[i] - delta*u[i-1])/p[i];
	}

	M[0] = 0;
	M[size] = 0;
	M[size-1] = u[size-1];
	for( int i = size-2; i >= 1; i-- ) {
		M[i] = u[i] + q[i]*M[i+1];
	}

}

double NFSI3TransformationBW::s(double x, vector<int>& row) {
	const int k = ceil(x);
	const int x_k = k;
	double result = ( (1/6)*M[k-1]*pow(x_k-x,3) + (1/6)*M[k]*pow(x-(x_k-1), 3) + (row[x_k-1] - (1/6)*M[k-1]) *(x_k-x) + (row[x_k] - (1/6)*M[k])*(x-(x_k-1)) );
	return result;
}

void NFSI3TransformationBW::scaleX(int newSize) {
	for( unsigned int i = 0; i < image->size(); i++) {
		transformRow( image->at(i), newSize );
	}
}

void NFSI3TransformationBW::scaleY(int newSize) {
	vector< vector<int> > oldImage = (*image);
	int oldSize = oldImage.size();
	int firstRowSize = oldImage.at(0).size();

	image->resize(newSize);

	for(int x = 0; x < firstRowSize; x++) {
		vector<int> oldRow = createRow(oldImage, x);
		prepareValuesForNFSI3(oldRow);

		for(int y = 0; y < newSize;y++) {
			(*image)[y].resize(firstRowSize);
			double pi_r = pi( y, newSize, oldSize);
			if( floor(pi_r) == pi_r )
				(*image)[y][x] = oldImage.at(pi_r).at(x);
			else
				(*image)[y][x] = s(pi_r, oldRow);
		}
	}
}

vector<int> NFSI3TransformationBW::createRow(vector< vector<int> >& image, int x) {
	vector<int> row = vector<int>();
	for( unsigned int y = 0; y < image.size(); y++)
		row.push_back(image.at(y).at(x));

	return row;
}

void NFSI3TransformationBW::scaleXY(int newX, int newY) {
	scaleX(newX);
	scaleY(newY);
}

void NFSI3TransformationBW::scaleYX(int newX, int newY) {
	scaleY(newY);
	scaleX(newX);
}

NFSI3TransformationBW::~NFSI3TransformationBW() {
}
