#ifndef NFSI3TRANSFORMATIONBW_HPP_
#define NFSI3TRANSFORMATIONBW_HPP_

#include "transformationbw.hpp"
#include <vector>

class NFSI3TransformationBW: public TransformationBW {
private:
	std::vector< std::vector<int> >* image;
	std::vector<double> p, q, u, d, M;

public:
	NFSI3TransformationBW(std::vector< std::vector<int> >* image);
	virtual ~NFSI3TransformationBW();
	void scaleX(int newSize);
	void scaleY(int newSize);
	void scaleXY(int newX, int newY);
	void scaleYX(int newX, int newY);

private:
	void transformRow( std::vector<int>& row, int newSize );
	double pi( int i, int newSize, int oldSize);
	void prepareValuesForNFSI3(std::vector<int>& row);
	double s( double x, std::vector<int>& row );
	std::vector<int> createRow( std::vector< std::vector<int> >& image, int x );
};

#endif /* NFSI3TRANSFORMATIONBW_HPP_ */
