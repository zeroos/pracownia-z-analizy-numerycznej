#ifndef NFSI1TRANSFORMATIONBW_HPP_
#define NFSI1TRANSFORMATIONBW_HPP_
#include "transformationbw.hpp"
#include <vector>

class NFSI1TransformationBW: TransformationBW {
private:
	std::vector< std::vector<int> >* image;

public:
	NFSI1TransformationBW(std::vector< std::vector<int> >* image);
	virtual ~NFSI1TransformationBW();
	void scaleX(int newSize);
	void scaleY(int newSize);
	void scaleXY(int newX, int newY);
	void scaleYX(int newX, int newY);


private:
	void transformRow( std::vector<int>& row, int newSize );
	double pi( int i, int newSize, int oldSize);
};

#endif /* NFSI1TRANSFORMATIONBW_HPP_ */
