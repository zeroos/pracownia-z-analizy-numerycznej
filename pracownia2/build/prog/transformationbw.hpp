#ifndef TRANSFORMATIONBW_HPP_
#define TRANSFORMATIONBW_HPP_

class TransformationBW {
public:
	virtual void scaleX(int newSize) = 0;
	virtual void scaleY(int newSize) = 0;
	virtual void scaleXY(int newX, int newY) = 0;
	virtual void scaleYX(int newX, int newY) = 0;
};



#endif /* TRANSFORMATIONBW_HPP_ */
