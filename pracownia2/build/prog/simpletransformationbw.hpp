#ifndef SIMPLETRANSFORMATIONBW_HPP_
#define SIMPLETRANSFORMATIONBW_HPP_
#include "transformationbw.hpp"
#include <vector>

class SimpleTransformationBW: TransformationBW {
private:
	std::vector< std::vector<int> >* image;

public:
	SimpleTransformationBW(std::vector< std::vector<int> >* image);
	virtual ~SimpleTransformationBW();
	void scaleX(int newSize);
	void scaleY(int newSize);
	void scaleXY(int newX, int newY);
	void scaleYX(int newX, int newY);


private:
	void transformRow( std::vector<int>& row, int newSize );
	int roundpi( int i, int newSize, int oldSize);
};

#endif /* SIMPLETRANSFORMATIONBW_HPP_ */
