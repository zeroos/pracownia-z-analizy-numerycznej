#include <Magick++/Color.h>
#include <Magick++/Geometry.h>
#include <Magick++/Image.h>
//#include <Magick++.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

#include "nfsi1transformationbw.hpp"
#include "nfsi3transformationbw.hpp"
#include "simpletransformationbw.hpp"

using namespace std;
using namespace Magick;

void usage(){
    cout << "Sposob uzycia:\t./scale plik_wejsciowy algorytm szerokosc wysokosc [plik_wyjsciowy]" << endl
         << endl
         << "Gdzie algorytm może być jednym z:" << endl
         << "\tsimple" << endl
         << "\tNFSI1" << endl
         << "\tNFSI3" << endl
         << endl
         << "Jesli plik wyjsciowy nie zostanie podany to wynik zostanie wyswietlony w nowym oknie" << endl;
}

void previewVectorImage(vector< vector<int> > vectorImage)
{   
    cout << " --- preview - begining [" << vectorImage.size() << "x" << vectorImage.at(0).size() << "]--- " << endl;
    for(unsigned int y=0; y<vectorImage.size(); y++){
        for(unsigned int x=0; x<vectorImage.at(y).size(); x++){
            cout << vectorImage.at(y).at(x) << " ";
        }
        cout << endl;
    }
    cout << " --- preview - end --- " << endl;
}

int main( int argc, char ** argv) 
{
    if(argc < 5){
        usage();
        return 1;
    }
    string input_file = argv[1];
    string algorithm = argv[2];
    int width = atoi(argv[3]);
    int height = atoi(argv[4]);
    string output_file = "";

    if(argc > 5){//will be replaced by getopts or sth
        output_file = argv[5];
    }
    InitializeMagick(*argv);
    Image image = Image(input_file);
    cout << "Zaladowano plik '" << input_file << "'." << endl;
    cout << "Zostanie on przeskalowany przy uzyciu algorytmu '" << algorithm 
         << "' do rozdzielczosci " << width << "x" << height << "." << endl;




    //transform Magick++ image to vectorImage
    vector< vector<int> > vectorImageR;
    vector< vector<int> > vectorImageG;
    vector< vector<int> > vectorImageB;
    for(unsigned int y=0; y<image.rows(); y++){
        vector<int> rowR;
        vector<int> rowG;
        vector<int> rowB;
        for(unsigned int x=0; x<image.columns(); x++){
            Color color = image.pixelColor(x,y);
            rowR.push_back((int)color.redQuantum());
            rowG.push_back((int)color.greenQuantum());
            rowB.push_back((int)color.blueQuantum());
        }
        vectorImageR.push_back(rowR);
        vectorImageG.push_back(rowG);
        vectorImageB.push_back(rowB);
    }

    //transform it
    if(algorithm.compare("simple") == 0){
    	SimpleTransformationBW transformationR(&vectorImageR);
    	SimpleTransformationBW transformationG(&vectorImageG);
    	SimpleTransformationBW transformationB(&vectorImageB);
    	transformationR.scaleXY(width, height);
    	transformationG.scaleXY(width, height);
    	transformationB.scaleXY(width, height);
    }else if(algorithm.compare("NFSI1") == 0){
    	NFSI1TransformationBW transformationR(&vectorImageR);
    	NFSI1TransformationBW transformationG(&vectorImageG);
    	NFSI1TransformationBW transformationB(&vectorImageB);
    	transformationR.scaleXY(width, height);
    	transformationG.scaleXY(width, height);
    	transformationB.scaleXY(width, height);
    }else if(algorithm.compare("NFSI3") == 0){
    	NFSI3TransformationBW transformationR(&vectorImageR);
    	NFSI3TransformationBW transformationG(&vectorImageG);
    	NFSI3TransformationBW transformationB(&vectorImageB);
    	transformationR.scaleXY(width, height);
    	transformationG.scaleXY(width, height);
    	transformationB.scaleXY(width, height);
    }else{
        cout << "Nieobslugiwany algorytm skalowania." << endl;
        usage();
        return 2;
    }
    
    
    //transform it back to Magick++
    Image outputImage = Image(Geometry(width, height), "pink");
    for(unsigned int y=0; y<vectorImageR.size(); y++){
        for(unsigned int x=0; x<vectorImageR.at(y).size(); x++){
            outputImage.pixelColor(x, y, Color(
                                               vectorImageR.at(y).at(x), 
                                               vectorImageG.at(y).at(x), 
                                               vectorImageB.at(y).at(x)
                                              )
            );
        }
    }




    if(output_file.compare("") == 0){
        cout << "Wyswietlam wynik w nowym oknie." << endl;
        outputImage.display();
    }else{
        outputImage.write(output_file);
        cout << "Plik zostal zapisany jako " << output_file << "." << endl;
    }
    return 0;
}
