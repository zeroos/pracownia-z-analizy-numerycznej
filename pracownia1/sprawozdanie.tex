\documentclass[11pt,wide]{mwart}
\usepackage[utf8]{inputenc}
\usepackage[OT4,plmath]{polski}
\usepackage{hyperref}
\usepackage{bm}
\usepackage{floatrow}
\usepackage{amsfonts}

\title{\textbf{Pracownia z analizy numerycznej}\\[1ex]
	   \large{Sprawozdanie do zadania \textbf{P1.20}}\\[0.5ex]
	   \large{Prowadzący: dr Paweł Woźny}
}

\author{Barciś Michał\\
		Chabinka Tomasz}
\date{Wrocław, \today}
\usepackage{graphicx}
\usepackage{amsmath}

\begin{document}

\maketitle

\section{Wstęp}
\label{Wstep}
Trudno wyobrazić sobie dziś dalszy rozwój nauk technicznych bez możliwości swobodnego wykorzystywania w obliczeniach komputerowych funkcji trygonometrycznych, a wśród nich sinusa. Funkcja ta wykorzystywana jest w tak rozległych obszarach wiedzy jak elektronika, fizyka kwantowa czy geometria obliczeniowa. Jednocześnie nie jest znany jawny wzór pozwalający obliczyć dokładną wartość sinusa w zadanym punkcie. Komputerowa implementacja sinusa może zaoferować jedynie obliczanie pewnego jej przybliżenia.

Celem niniejszej pracy jest zbadanie wykorzystywanych współczenie algorytmów obliczania sinusa z wykorzystaniem jedynie podstawowych operacji arytmetycznych - dodawania, odejmowania, mnożenia i dzielenia. Pokazane zostanie, jak metody obliczania sinusa stosowane zazwyczaj w dziedzinie liczb rzeczywistych sprawdzają się w dziedzinie liczb zespolonych. Zaproponowane zostaną pewne ulepszenia pozwalające obliczać sinus liczb zespolonych z większą dokładnością.

Główny wywód pracy związany będzie z metodą obliczania przybliżenia sinusa opartą o ciąg Taylora. W sekcji \ref{OpisAlgorytmu} zaprezentowane zostaną wyniki przeprowadzonych testów oraz interpretacja otrzymanych wyników. W sekcji \ref{tailor_udoskonalenia} zaproponowane zostaną udoskonalenia, które pozwolą na poprawę osiąganych wyników.

W dalszej części pracy zaprezentujemy inną metodę obliczania sinusa, która mimo większej złożoności obliczeniowej jest często używana ze względu na swoje specyficzne właściwości, które zostaną przybliżone w sekcji \ref{CORDIC}.

Wszystkie testy numeryczne przeprowadzono przy użyciu systemu algebry komputerowej \textsf{Maple 17} symulując tryb pojedynczej (\texttt{Single}), albo podwójnej precyzji (\texttt{Double}) obliczeń, tj. przyjmując odpowiednio \texttt{Digits := 8} i \texttt{Digits := 16}.

\section{Szereg Taylora}
\label{SzeregTaylora}

\subsection{Opis algorytmu}
\label{OpisAlgorytmu}
Wzorem Taylora nazywamy przedstawienie funkcji (n+1)-razy różniczkowalnej za pomocą wielomianu zależnego od kolejnych jej pochodnych oraz dostatecznie małej reszty. Ponieważ $\sin' x = \cos x$ oraz $\cos' x = -\sin x$ to sinus jest funkcją n-razy różniczkowalną dla dowolnego n. 

Szereg Taylora ma dla tej funkcji postać: \[ \sin x = \sum_{k=0}^\infty \frac{(-1)^k}{(2k+1)!}x^{2k+1} \] 

Równoważnie dla pewnego n: \[ \sin x = \sum_{k=0}^n \frac{(-1)^k}{(2k+1)!}x^{2k+1} + R_n(x) \]

Gdzie: \[ R_n(x)= \frac{x^{n+1}}{(n+1)!}f^{(n+1)}(x^*), x^* \in \left\{ 0 \ldots x\right\}  \]

Ponieważ wartość $ -1 \leq f^{(n+1)}(x^*) \leq 1 $ to $ -\frac{x^{n+1}}{(n+1)!} \leq R_n(x) \leq \frac{x^{n+1}}{(n+1)!} $ co oznacza, że reszta $R_n$ bardzo szybko (dla małego n) staje się pomijalnie mała. Wydaje się więc, że obliczanie przybliżenia wartości $\sin x$ za pomocą szeregu Taylora jest uzasadnione. Tabela \ref{T:Tab1}. przedstawia błąd względny przy obliczaniu $\sin x$ w precyzji Single i Double za pomocą pierwszych 10 wyrazów szeregu Taylora. Tabela \ref{T:Tab2} przedstawia błąd bezwzględny wyniku obliczeń dla argumentów dla których $\sin x = 0$.

\begin{table}[!h]
\renewcommand{\arraystretch}{1.1}
\begin{center}

\caption{Przybliżony błąd względny przy obliczaniu $sin(x)$ w precyzji \texttt{Single} i \texttt{Double}  za pomocą pierwszych 10 wyrazów szeregu Taylora}
\label{T:Tab1}
\vspace{1ex}
\begin{center}
\begin{tabular}{r|l|l|l}
$x$ & \texttt{Błąd względny (Single)} & \texttt{Błąd względny (Double)} & \texttt{|x|} \\ \hline
$0$ & $0$ & $0$ & 0 \\  
$i$ & $5.40860 \cdot 10^{-9}$ & $3.88770 \cdot 10^{-16}$ & $1$ \\ 
$1.2345678$ & $4.96722 \cdot 10^{-9}$ & $0$ & $1.23$ \\ 
$1+1i$ & $1.32767 \cdot 10^{-8}$ & $2.04056 \cdot 10^{-16}$ & $1.41$ \\ 
$3-3i$ & $4.30853 \cdot 10^{-9}$ & $1.052403 \cdot 10^{-9}$ & $4.24$ \\ 
$-5+5i$ & $0.0000179468$ & $0.000017952801616$ & $7.07$ \\ 
$-6+6i$ & $0.000436403$ & $0.0004363930087679$ & $8.48$ \\ 
$4+25i$ & $0.85140003$ & $0.8514000259775089$ & $25.31$ \\ 
$5 – 30i$ & $1.01568687$ & $1.0156868702182835$ & $30.41$ \\ 
$-7 + 45i$ & $1.0000712316$ & $1.0000712316202788015$ & $45.54$ \\ 
$-10-50i$ & $0.9999940619$ & $0.9999940619095175735$ & $50.99$ \\ 
$56.43 – 23.67i$ & $6.30328 \cdot 10^7$ & $6.30328 \cdot 10^7$ & $61.19$ \\ 
$100i$ & $0.99999999999$ & $0.99999999999$ & $100$ \\ 
$100 + 100i$ & $1$ & $1$ & $141.42$ \\ 
$100\pi + 100i$ & $0.99999999999$ & $0.99999999997913$ & $329.69$ \\ 
$102Pi + 100i$ & $1$ & $1$ & $420.44$ \\
\end{tabular}
\end{center}
\end{center}
\end{table}

\begin{table}[!h]
\label{table_taylor2}
\renewcommand{\arraystretch}{1.1}
\begin{center}

\caption{Przybliżony błąd bezwzględny przy obliczaniu $\sin(x)$ w precyzji \texttt{Single} i \texttt{Double}  za pomocą pierwszych 10 wyrazów szeregu Taylora}
\label{T:Tab2}
\vspace{1ex}
\begin{center}
\begin{tabular}{r|l|l|l}
$x$ & \texttt{Błąd względny (Single)} & \texttt{Błąd względny (Double)} & \texttt{|x|} \\ \hline
$0$ & $0$ & $0$ & 0 \\  
$2\pi$ & 0.000082833801 & 0.00008274095220950644 & $6.28$ \\ 
$10\pi$ & $3.7138987*10^{11}$ & $3.713897509395682*10^{11}$ & $31.41$ \\ 
$100\pi$ & $5.3697972*10^{32}$ & $5.369795535462897*10^{32}$ & $314.15$ \\ 
\end{tabular}
\end{center}
\end{center}
\end{table}

Zauważyć można związek między wielkością błędu (tak względnego jak i bezwzględnego) a wartością bezwzględną argumentu $\sin |x|$.

\subsection{Udoskonalenia}
\label{tailor_udoskonalenia}

\begin{figure}[htp]
\centering
\includegraphics[scale=1.00]{sincondition.jpg}
\caption{Uwarunkowanie funkcji $\sin x = x\cdot \ctg x$}
\floatfoot{Źródło: WolframAlpha \cite{Wolfram|Alpha}}
\label{sin_condition}
\end{figure}

Wzrost błędu wraz ze wzrostem wielkości bezwzględnej argumentu $\sin |x|$ tłumaczyć można dwoma zjawiskami:
\begin{enumerate}
\item wolniejszą zbieżnością do prawidłowego wyniku sumy $\sin x = \sum_{k=0}^n \frac{(-1)^k}{(2k+1)!}x^{2k+1}$
\item uwarunkowaniem funkcji $\sin x$ (Rysunek \ref{sin_condition}.)
\end{enumerate}

Ad 1.) Zauważmy, że dla dużych wartości x wartości pierwszych wyrazów ciągu $\sin x = \sum_{k=0}^n \frac{(-1)^k}{(2k+1)!}x^{2k+1}$ będą duże - wartość $x^{2k+1}$ jest wielokrotnie większa od wartości $(2k+1)!$ dla dużego x i małego k. Szczególnie dobrze widać to w przypadku argumentów będących wielokrotnością $2\pi$ (patrz: Tabela \ref{T:Tab2}.) Rozwiązaniem tego problemu jest wykorzystanie okresowości funkcji $\sin x$ w dziedzinie liczb rzeczywistych: \[ \sin x = \sin r,\hspace{2 mm}x = 2k +r\hspace{2 mm}(k \in \mathbb{N}, r \in \left\{ 0 \ldots 2\pi\right\}) \]

Wtedy: \[ \sin x = \sum_{k=0}^\infty \frac{(-1)^k}{(2k+1)!}(x \bmod 2\pi)^{2k+1} \]

Ad 2.) Ponieważ uwarunkowanie funkcji $\sin (x + iy)$ rośnie wraz ze wzrostem wartości $\Im{(x + iy)} = y$ należy przekształcić używany dotychczas wzór, tak by obliczać wartość funkcji $\sin (x+iy)$ dla niewielkich wartości $y$. Można do tego wykorzystać powszechnie znane przekształcenia: 
\[ \sin (x + iy) = \sin x * \cosh y + i \cos x * \sinh y\] 
\[\sinh 2x = 2\sinh x * \cosh x\] 
\[\cosh 2x = \sinh^2 x * \cosh^2 x \] 
oraz wzór na szereg Taylora dla poszczególnych funkcji: 
\[ \cos x = \sum_{k=0}^\infty \frac{(-1)^k}{(2k)!}x^{2k} \] 
\[ \sinh x = \sum_{k=0}^\infty \frac{x^{2k+1}}{(2k+1)!} \] 
\[ \cosh x = \sum_{k=0}^\infty \frac{x^{2k}}{(2k)!} \]

Po uwzględnieniu tych ulepszeń nasz algorytm obliczania wartości funkcji $\sin x$ przyjmie formę: 
\[ \sin (x + iy) = \sin x * \cosh y + i \cos x * \sinh y \] gdzie:  
\[\sin x = \sum_{k=0}^\infty \frac{(-1)^k}{(2k+1)!}(x \bmod 2\pi)^{2k+1} \] 
\[\cos x = \sum_{k=0}^\infty \frac{(-1)^k}{(2k)!}(x \bmod 2\pi)^{2k} \] 
\[ \sinh x =
\left\{
\begin{array}{ll}
		2\sinh \frac{x}{2} * \cosh \frac{x}{2} & \mbox{if } x \geq 2 \\
		 \sum_{k=0}^\infty \frac{x^{2k+1}}{(2k+1)!} & \mbox{if } x < 2
	\end{array}
\right. \]
\[ \cosh x =
\left\{
\begin{array}{ll}
		\sinh (\frac{x}{2})^2 * \cosh (\frac{x}{2})^2 & \mbox{if } x \geq 2 \\
		 \sum_{k=0}^\infty \frac{x^{2k}}{2k!} & \mbox{if } x < 2
	\end{array}
\right. \]

Tabela \ref{T:Tab3}. przedstawia przybliżony błąd względny przy obliczaniu $\sin x$ w precyzji Single i Double za pomocą powyższego algorytmu. Wielkość błędu zależy obecnie od wielkości $|\Im(x)|$ - za pomocą operacji modulo udało się wyeliminować wpływ wielkości części rzeczywistej. 

Zastosowanie algorytmu rekurencyjnego do obliczania $\sinh x$ oraz $\cosh x$ pozwoliło zmniejszyć błąd względny spowodowany uwarunkowaniem obliczania wartości $\sin x$.

Najpoważniejszą wadą zaproponowanego rozwiązania jest wykładniczy wzrost wywołań rekurencyjnych funkcji $\sinh x$ i $\cosh x$ następujący wraz ze wzrostem wartości $|\Im(x)|$, co sprawia że program wykonuje się bardzo wolno dla małych wartości. Na przykład dla $|\Im(x)| = 1024$ drzewo wywołania rekurencyjnego będzie miało 1024 liści i 2047 węzłów.

Problem ten można jednak łatwo rozwiązać gdy zauważymy, że w każdej z gałęzi tego drzewa liczymy wartości funkcji $\sinh$ i $\cosh$ dla tych samych argumentów. Wystarczy spamiętywać wyniki tych obliczeń aby wzrost tych wywołań rekurencyjnych był liniowy.

\begin{table}[!h]
\renewcommand{\arraystretch}{1.1}
\begin{center}

\caption{Przybliżony błąd względny przy obliczaniu $sin(x)$ w precyzji \texttt{Single} i \texttt{Double}  za pomocą algorytmu rekurencyjnego}
\label{T:Tab3}
\vspace{1ex}
\begin{center}
\begin{tabular}{r|l|l|l}
$x$ & \texttt{Błąd względny (Single)} & \texttt{Błąd względny (Double)} & $|\Im(x)|$ \\ \hline
$0$ & $0$ & $0$ & 0 \\
$2\pi$ & $0$ & $0$ & $0$ \\ 
$10\pi$ & $0$ & $0$ & $0$ \\ 
$100\pi$ & $0$ & $0$ & $0$ \\ 
$1.2345678$ & $4.96722 \cdot 10^{-9}$ & $0$ & $0$ \\ 
$i$ & $5.40860 \cdot 10^{-9}$ & $3.88770 \cdot 10^{-16}$ & $1$ \\ 
$1+1i$ & $5.64432 \cdot 10^{-8}$ & $2.95108 \cdot 10^{-16}$ & $1$ \\ 
$3-3i$ & $1.06860 \cdot 10^{-8}$ & $2.77034 \cdot 10^{-11}$ & $3$ \\ 
$-5+5i$ & $9.35619 \cdot 10^{-8}$ & $9.15784 \cdot 10^{-16}$ & $5$ \\
$-6+6i$ & $9.93428 \cdot 10^{-8}$ & $1.23591 \cdot 10^{-15}$ & $6$ \\ 
$56.43 – 23.67i$ & $1.25293 \cdot 10^{-6}$ & $8.37893 \cdot 10^{-15}$ & $23.67$ \\
$4+25i$ & $1.11469 \cdot 10^{-7}$ & $1.14604 \cdot 10^{-16}$ & $25$ \\ 
$5 – 30i$ & $9.97818 \cdot 10^{-8}$ & $2.92336 \cdot 10^{-15}$ & $30$ \\ 
$-7 + 45i$ & $4.85749 \cdot 10^{-7}$ & $4.93445 \cdot 10^{-15}$ & $45$ \\ 
$-10-50i$ & $3.77445 \cdot 10^{-7}$ & $9.00404 \cdot 10^{-13}$ & $50$ \\  
$100i$ & $1.27158 \cdot 10^{-7}$ & $7.97897 \cdot 10^{-15}$ & $100$ \\ 
$100 + 100i$ & $5.074045 \cdot 10^{-6}$ & $1.8 \cdot 10^{-14}$ & $100$ \\ 
$100\pi + 100i$ & $1.27158 \cdot 10^{-7}$ & $7.97897 \cdot 10^{-15}$ & $100$ \\ 
$102\pi + 100i$ & $1.27158 \cdot 10^{-7}$ & $7.97897 \cdot 10^{-15}$ & $100$ \\
\end{tabular}
\end{center}
\end{center}
\end{table}

\section{CORDIC}
\label{CORDIC}

\textbf{CORDIC} (\textbf{CO}ordinate \textbf{R}otation \textbf{DI}gital \textbf{C}omputer) to metoda służąca do obliczania wartości funkcji trygonometrycznych i hiperbolicznych opisana w 1959 roku przez Jacka Voldera. Główną jej zaletą jest prostota --- wykorzystuje jedynie operacje dodawania, odejmowania, dzielenia przez dwa (przesunięcia bitowe, jeśli liczba reprezentowana jest w systemie dwójkowym) oraz tablicowanie. Dzięki temu jest idealnym rozwiązaniem dla komputerów o ograniczonych możliwościach, w których priorytetem są niskie koszty produkcji, np. kalkulatorach lub systemach wbudowanych. Algorytm ten, ze względu na swoją prostotę, często implementuje się sprzętowo. 

\subsection{Schemat działania}

Jednym z zastosowań metody CORDIC jest obliczanie wartości funkcji trygonometrycznych. Po jednorazowym wykonaniu algorytmu możemy równocześnie otrzymać wartości sinusa i cosinusa danego kąta, a także, korzystając z odpowiednich przekształceń, pozostałych funkcji trygonometrycznych.

\begin{figure}[htp]
\centering
\includegraphics[scale=1.00]{cordicillustration.png}
\caption{Schemat działania metody CORDIC}
\label{cordic_ilustracja}
\floatfoot{Źródło: Wikipedia\cite{cordic_illustraion_wikipedia_url}}
\end{figure}

Rysunek \ref{cordic_ilustracja} prezentuje przebieg algorytmu wykorzystanego do odnalezienia wartości funkcji $\sin(\beta)$ i $\cos(\beta)$. Na początku wybieramy wektor 
$$
v_0 = 
\begin{bmatrix}
	1 \\
	0 \\
\end{bmatrix}
$$ długości 1. W kolejnych krokach algorytmu będziemy obracać wektor $v_i$ o coraz mniesze kąty $\gamma_i$ w odpowiednim kierunku tak, aby za każdym razem uzyskiwać kąt coraz bliższy kątowi $\beta$. Współrzędne wektora po takim obrocie otrzymujemy ze wzoru: $$v_i = R_iv_{i-1},$$ gdzie $R_i$ jest macierzą obrotu zadaną wzorem:
$$
R_i = 
\begin{bmatrix}
	\cos(\gamma_i) & -\sin(\gamma_i)\\
	\sin(\gamma_i) & \cos(\gamma_i)\\
\end{bmatrix}.
$$
Po wykonaniu odpowienich przekształceń\cite{cordic_wikipedia_url} otrzymujemy:
$$
v_i = {1 \over {1 + \tan^2\gamma_i}} 
\begin{bmatrix}
	1 & -\tan\gamma_i\\
	\tan\gamma_i & 1\\
\end{bmatrix}
v_{i-1}.
$$
Jak widzimy do wyliczenia współrzędnych każdego następnego wektora $v_i$ musimy wykonać tylko operacje dodawania i mnożenia, oraz znać wartości funkcji $\tan(\gamma_i)$. Jednak możemy tak dobrać kąty $\gamma_i$, że $\tan\gamma_i = 2^{-i}$, a operację dzielenia przez dwa na liczbach zapisanych w systemie dwójkowym można wykonać używając przesunięcia bitowego.

Łatwo pokazać, że każdy taki krok w najgorszym wypadku zmniejsza błąd bezwzględny wyniku o połowę\cite{cordic_ucl_url}. W związku z tym algorytm jest zbieżny.

\subsection{Udoskonalenia}

Aby usprawnić działanie algorytmu CORDIC można zastosować analogiczne udoskonalenia do tych, które opisane były w sekcji \ref{tailor_udoskonalenia}.

\subsection{Zastosowania}

Mimo stosunkowo prostego pomysłu na którym oparty jest CORDIC jego możliwości i zastosowania są zaskakująco bogate. Przy niewielkich modyfikacjach algorytmu polegających m. in. na zmianie wartości początkowej wektora lub wzorów wykorzystywanych do rotacji można obliczyć wiele często używanych wartości\cite{cordic_usecases}:

\begin{itemize}
\item funkcji trygonometrycznych,
\item funkcji hiperbolicznych,
\item funkcji wykładniczych,
\item pierwiastków kwadratowych,
\item logarytmów,
\item mnożenia,
\item dzielenia.
\end{itemize}

Dzięki temu algorytm ten jest tak chętnie używany w prostych układach elektronicznych - ten sam układ może być wielokronie wykorzystywany.

Jednak w urządzeniach, które mają możliwość sprzętowego obliczania iloczynu, wykorzystanie Szeregu Tailora jest bardziej efektywne. W związku z tym zastosowania algorytmu CORDIC ograniczają się do rozwiązań czysto sprzętowych w których ważna jest minimalizacja liczby podzespołów.

\section{Wnioski}

W niniejszej pracy przedstawione zostały wyniki podjętych przez nas prób praktycznej implementacji dwóch najpopularniejszych rozwiązań znajdowania wartości sinusa: opartego o wzór Taylora (Sekcja \ref{SzeregTaylora}.) i opartego o algorytm CORDIC (Sekcja \ref{CORDIC}). Wychodząc od proponowanych w literaturze rozwiązań wykonaliśmy szereg eksperymentów sprawdzających ich numeryczną jakość (Sekcja \ref{OpisAlgorytmu}). Wykonane testy wskazały na dużą niedokładność klasycznej implementacji tych algorytmów. W efekcie zaproponowaliśmy trzy poprawki implementacyjne (Sekcja \ref{tailor_udoskonalenia}), które pozwoliły na istotną poprawę uzyskiwanych w trakcie prowadzonych eksperymentów wyników.

Ponadto w trakcie testów zauważona została istotna przewaga - pod względem dokładności uzyskiwanych wyników - algorytmu opartego o wzór Taylora nad algorytmem CORDIC. Zaproponowana przez nas ostateczna implementacja pierwszego z nich osiągnęła precyzję bliską precyzji arytmetyki przy złożoności czasowej równej $\lg \Im(n) $, co pozwala uznać że w niniejszej pracy udało się zaproponować implementację, która może być wykorzystana nie tylko w teorii, ale również w praktyce.

\section{Dodatek: Uruchamianie programu}

Program składa się z kilku plików źródłowych:
\begin{itemize}
\item program.mpl (odpowiednio program.mw) - główny plik programu,
\item cordic.mpl (cordinc.mw) - plik implementujący algorytm CORDIC,
\item tailor.mpl (tailor.mw) - plik implementujący algorytm oparty na wzorze Taylora,
\item trygutils.mpl (trygutils.mw) - plik pomocniczy implementujący algorytm rekurencyjny.
\end{itemize}

Uruchomienie programu następuje poprzez wczytanie pliku program.mpl i wykonanie wszystkich znajdujących się w nim komend. Użytkownik otrzymuje wówczas interfejs składający się z poleceń:

\begin{itemize}
\item tailorSin,
\item tailorCos,
\item cordicSin,
\item cordicCos,
\end{itemize}

kóre może wykorzystać do przetestowania zaimplementowanych algorytmów. Funkcje należy wykonać z dwoma argumentami - pierwszy to wartość kąta dla którego chcemy policzyć daną funkcję, a drugi to liczba iteracji, które algorytm ma wykonać. Ponadto plik program.mpl zawiera wykresy obrazujące wyniki przeprowadzonych testów.

\begin{thebibliography}{9}

\bibitem{cordic_ucl_url}
  \url{http://www.cs.ucla.edu/digital_arithmetic/files/ch11.pdf}

\bibitem{cordic_wikipedia_url}
  \url{http://en.wikipedia.org/wiki/CORDIC}

\bibitem{cordic_usecases}
  \url{http://archives.math.utk.edu/ICTCM/VOL11/C027/paper.pdf}

  
\bibitem{Wolfram|Alpha}
  \url{http://www.wolframalpha.com/input/?i=%28a%2Bbi%29*+cot%28a%2Bbi%29}

\bibitem{cordic_illustraion_wikipedia_url}
  \url{http://en.wikipedia.org/wiki/File:CORDIC-illustration.png}

\end{thebibliography}


\end{document}