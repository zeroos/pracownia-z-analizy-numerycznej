Digits := 16;

%;
read "./tailor.mpl";
%;
read "./cordic.mpl";
%;

with(plots);
# 
# Uwarunkowanie funkcji sinus w dziedzinie liczb zespolonych:
densityplot(proc (x, y) options operator, arrow; Re((x+I*y)*cot(x+I*y)) end proc, -10 .. 10, -10 .. 10, colorstyle = HUE);



%;
# 
# Wykresy wartości bezwzględnej, części rzeczywistej i urojonej funkcji sinus policzonej za pomocą szerego Tailora w dziedzinie liczb zespolonych:
# 
plot3d(proc (x, y) options operator, arrow; abs(complexTailorSin(x+I*y, 20)) end proc, -3 .. 3, -3 .. 3);
%;
plot3d(proc (x, y) options operator, arrow; Re(complexTailorSin(x+I*y, 20)) end proc, -10 .. 10, -3 .. 3);
%;
plot3d(proc (x, y) options operator, arrow; Im(complexTailorSin(x+I*y, 20)) end proc, -10 .. 10, -3 .. 3);
%;
# 
# 
# 
# 
# Sinus w dziedzinie liczb rzeczywistych policzony za pomocą metody CORDIC:
plot(proc (x) options operator, arrow; cordicSin(x, 100) end proc, -10 .. 10, -1.5 .. 1.5);
%;
# Oraz wartość urojona wyniku w dziedzinie liczb zespolonych:
plot3d(proc (x, y) options operator, arrow; Im(complexCordicSin(x+I*y, 20)) end proc, -10 .. 10, -3 .. 3);
%;
# 
