modulo := proc (a, b) local y; y := a-evalf(floor(evalf(a/b))*b); if evalf(Pi) < y then y := y-2*Pi end if; y end proc; complexSin := proc (customSin, customCos, customSinh, customCosh) proc (x, iterations) options operator, arrow; customSin(Re(modulo(x, evalf(2*Pi))), iterations)*customCosh(Im(x), iterations)+I*customCos(Re(modulo(x, evalf(2*Pi))), iterations)*customSinh(Im(x), iterations) end proc end proc;
# 
complexSinFrem := proc (customSin, customCos, customSinh, customCosh) proc (x, iterations) options operator, arrow; customSin(Re(frem(x, evalf(2*Pi))), iterations)*customCosh(Im(x), iterations)+I*customCos(Re(frem(x, evalf(2*Pi))), iterations)*customSinh(Im(x), iterations) end proc end proc;
# 
# 
modulo(3, evalf(2*Pi));
plot(proc (x) options operator, arrow; frem(x, evalf(2*Pi))-modulo(x, evalf(2*Pi)) end proc, -100 .. 100);
%;

%;
# 
