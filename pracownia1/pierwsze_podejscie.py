from math import factorial
import math


def sin(x, n=50):
    result = 0.0
    for i in range(n):
        partial = (((-1.0)**i) / factorial(2.0*i+1.0))
        result += partial*(x**(2.0*i+1.0))
    return result
