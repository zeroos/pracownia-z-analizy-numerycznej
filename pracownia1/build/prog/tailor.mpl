read "./trygutils.mpl";

tailorSinhCache := table();
tailorCoshCache := table();

tailorSin := proc (x, iterations) local result, i; result := 1; for i from iterations by -1 to 1 do result := evalf(1-(1/2)*x^2*result/(i*(2*i+1))) end do; result := evalf(result*x) end proc;
# 

NULL;

tailorCos := proc (x, iterations) local result, i; result := 1; for i from iterations by -1 to 1 do result := evalf(1-(1/2)*x^2*result/(i*(2*i-1))) end do; result := evalf(result) end proc;
%;


tailorSinh := proc (x, iterations) local result, i; global tailorSinhCache; result := 1; if tailorSinhCache[x] <> evaln(tailorSinhCache[eval(x)]) then tailorSinhCache[x] else for i from iterations by -1 to 1 do result := evalf(1+(1/2)*x^2*result/(i*(2*i+1))) end do; result := evalf(result*x); tailorSinhCache[x] := result end if end proc;
# 


tailorCosh := proc (x, iterations) local result, i; global tailorCoshCache; result := 1; if tailorCoshCache[x] <> evaln(tailorCoshCache[eval(x)]) then tailorCoshCache[x] else for i from iterations by -1 to 1 do result := evalf(1+(1/2)*x^2*result/(i*(2*i-1))) end do; result := evalf(result); tailorCoshCache[x] := result end if end proc;

tailorSinhRecursive := proc (x, iterations) local result; if 2 < abs(x) then result := 2*tailorSinhRecursive((1/2)*x, iterations)*tailorCoshRecursive((1/2)*x, iterations) else result := tailorSinh(x, iterations) end if; result end proc;
tailorCoshRecursive := proc (x, iterations) local result; if 2 < abs(x) then result := tailorCoshRecursive((1/2)*x, iterations)^2+tailorSinhRecursive((1/2)*x, iterations)^2 else result := tailorCosh(x, iterations) end if; result end proc;
complexTailorSin := complexSin(tailorSin, tailorCos, tailorSinhRecursive, tailorCoshRecursive);
%;


# 
