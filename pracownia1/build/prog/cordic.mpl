read "./trygutils.mpl";

# 
cordicCoshCache := table();
cordicSinhCache := table();
# 
cordic := proc (iterations, initialAngle, hyperbolic) local vx, vy, m, istart, angle, nextAngle, K, n, d, vxn, vyn; vx := 1; vy := 0; angle := initialAngle; if hyperbolic then m := -1; istart := 1 else m := 1; istart := 0 end if; K := evalf(product(1/sqrt(1+m*2^(-2*k)), k = istart .. iterations-1)); for n from istart to iterations do if angle < 0 then d := -1 else d := 1 end if; vxn := evalf(vx-vy*d*m*2^(-n)); vyn := evalf(vy+vx*d*2^(-n)); if hyperbolic then if n = 0 then nextAngle := angle else nextAngle := evalf(angle-d*arctanh(2^(-n))) end if else nextAngle := evalf(angle-d*arctan(2^(-n))) end if; vx := vxn; vy := vyn; angle := nextAngle end do; K*vx, K*vy, vy/vx end proc;

%;

cordicSin := proc (x, iterations) local angle; angle := frem(x, evalf(2*Pi)); if evalf((1/2)*Pi) < abs(angle) then angle := frem(x, evalf(Pi)); -cordic(iterations, angle, false)[2] else cordic(iterations, angle, false)[2] end if end proc;

cordicCos := proc (x, iterations) local angle; angle := frem(x, evalf(2*Pi)); if evalf((1/2)*Pi) < abs(angle) then angle := frem(angle, evalf(Pi)); -cordic(iterations, angle, false)[1] else cordic(iterations, angle, false)[1] end if end proc;


cordicSinh := proc (x, iterations) global cordicSinhCache; if cordicSinhCache[x] <> evaln(cordicSinhCache[eval(x)]) then cordicSinhCache[x] else cordicSinhCache[x] := cordic(iterations, x, true)[2] end if end proc;


cordicCosh := proc (x, iterations) global cordicCoshCache; if cordicCoshCache[x] <> evaln(cordicCoshCache[eval(x)]) then cordicCoshCache[x] else cordicCoshCache[x] := cordic(iterations, x, true)[1] end if end proc;

cordicSinhRecursive := proc (x, iterations) local result; if 1 < abs(x) then result := 2*cordicSinhRecursive((1/2)*x, iterations)*cordicCoshRecursive((1/2)*x, iterations) else result := cordicSinh(x, iterations) end if; result end proc;
cordicCoshRecursive := proc (x, iterations) local result; if 1 < abs(x) then result := cordicCoshRecursive((1/2)*x, iterations)^2+cordicSinhRecursive((1/2)*x, iterations)^2 else result := cordicCosh(x, iterations) end if; result end proc;


complexCordicSin := complexSin(cordicSin, cordicCos, cordicSinhRecursive, cordicCoshRecursive);
# 


