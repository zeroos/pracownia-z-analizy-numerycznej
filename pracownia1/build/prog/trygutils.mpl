complexSin := proc (customSin, customCos, customSinh, customCosh) proc (x, iterations) options operator, arrow; customSin(Re(frem(x, evalf(2*Pi))), iterations)*customCosh(Im(x), iterations)+I*customCos(Re(frem(x, evalf(2*Pi))), iterations)*customSinh(Im(x), iterations) end proc end proc;

